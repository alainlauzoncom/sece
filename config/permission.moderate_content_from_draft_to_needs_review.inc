<?php
/**
 * @file
 * permission.moderate_content_from_draft_to_needs_review.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'moderate content from draft to needs_review',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
    2 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
