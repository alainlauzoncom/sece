<?php
$form = array();

$form['milieu'] = array(
  '#weight' => -1,
  '#prefix' => '<div class="milieu">',
  '#suffix' => '</div>',
);
$form['milieu']['left'] = array(
  '#prefix' => '<div class="gauche">',
  '#suffix' => '</div>',
  'weight' => 0.1,
);
$form['milieu']['left']['warning'] = array(
  '#markup' => '<div class="btn btn-warning">Vous devez être inscrit au Système d\'échange de conférences en EDDEC (SECE) pour participer.</div>',
);

$form['milieu']['droit'] = drupal_get_form("user_login");

$form['milieu']['droit']['#prefix'] = '<div class="droit"><h4>Connexion utilisateur</h4>';
$form['milieu']['droit']['#suffix'] = '</div>';
$form['milieu']['droit']['weight'] = 0.2;

$form['milieu']['droit']['links']['#weight'] = 1000;
$form['milieu']['droit']['links']['#markup'] = '
  <div class="btn btn-check btn-primary">
      <div>Mot de passe oublié</div>
      <a href="/user/password" id="new-password-link"
         title="Demander un nouveau mot de passe par courriel.">
          Demander un nouveau mot de passe
      </a>
  </div>';
$form['milieu']['droit']['name']['#size'] = 25;
unset($form['milieu']['droit']['name']['#description']);
$form['milieu']['droit']['pass']['#size'] = 25;
unset($form['milieu']['droit']['pass']['#description']);


$form['bas']['register'] = array(
  '#weight' => 1000,
  '#markup' => '
        <a href="/user/register" class="btn btn-register btn-primary" id="register-link"
           title="S\'inscrire au SYSTÈME D\'ÉCHANGE DE CONFÉRENCE EN EDDEC.">
            S\'inscrire
        </a>',
  '#prefix' => '<div class="bas">',
  '#suffix' => '</div>',
);

echo drupal_render($form);
//echo '<pre> $form = ' . print_r($form, TRUE) . '</pre>';
drupal_add_js('/' . drupal_get_path('theme', 'orane') . '/assets/js/postProcessOverlay.js');
?>





