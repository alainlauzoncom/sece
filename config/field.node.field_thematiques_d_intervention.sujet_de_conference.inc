<?php
/**
 * @file
 * field.node.field_thematiques_d_intervention.sujet_de_conference.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'thematique_d_intervention_privilegiee',
          'parent' => '0',
        ),
      ),
      'options_list_callback' => 'title_taxonomy_allowed_values',
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_thematiques_d_intervention' => array(
              'tid' => 'field_thematiques_d_intervention_tid',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_thematiques_d_intervention' => array(
              'tid' => 'field_thematiques_d_intervention_tid',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(
      'tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array(
          'tid' => 'tid',
        ),
      ),
    ),
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'field_name' => 'field_thematiques_d_intervention',
    'type' => 'taxonomy_term_reference',
    'module' => 'taxonomy',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '-1',
    'deleted' => '0',
    'columns' => array(
      'tid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Thématiques',
    'widget' => array(
      'weight' => '3',
      'type' => 'options_buttons',
      'module' => 'options',
      'active' => 1,
      'settings' => array(
        'other' => 'Autre',
        'other_title' => 'Autre, précisez',
        'other_size' => '80',
        'sort_options' => 0,
        'available_options' => '',
        'other_unknown_defaults' => 'other',
      ),
    ),
    'settings' => array(
      'description_display' => 'before',
      'user_register_form' => FALSE,
    ),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'taxonomy_term_reference_plain',
        'weight' => '2',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'taxonomy',
      ),
      'full' => array(
        'label' => 'above',
        'type' => 'taxonomy_term_reference_link',
        'weight' => '2',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'taxonomy',
      ),
      'teaser' => array(
        'type' => 'hidden',
        'label' => 'above',
        'settings' => array(),
        'weight' => 0,
      ),
    ),
    'required' => 1,
    'description' => 'Vous pouvez sélectionner un nombre illimité de thématiques. Notez toutefois que plus vous serez précis, plus le maillage avec les autres utilisateurs du SECE sera facilité.',
    'default_value' => NULL,
    'field_name' => 'field_thematiques_d_intervention',
    'entity_type' => 'node',
    'bundle' => 'sujet_de_conference',
    'deleted' => '0',
  ),
);

$dependencies = array(
  'content_type.sujet_de_conference' => 'content_type.sujet_de_conference',
  'vocabulary.thematique_d_intervention_privilegiee' => 'vocabulary.thematique_d_intervention_privilegiee',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'taxonomy',
  2 => 'options',
);
