<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_conf_rence.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_conf_rence',
  'content' => array(
    '[conf_rence:uid]' => 0,
    '[conf_rence:changed]' => 0,
    '[conf_rence:field_confirme]' => 0,
    '[conf_rence:field-confirme]' => 0,
    '[conf_rence:created]' => 0,
    '[conf_rence:field_date]' => 0,
    '[conf_rence:field-date]' => 0,
    '[conf_rence:language]' => 0,
    '[conf_rence:field-heure]' => 0,
    '[conf_rence:field_heure]' => 0,
    '[conf_rence:field-hote]' => 0,
    '[conf_rence:field_hote]' => 0,
    '[conf_rence:id]' => 0,
    '[conf_rence:field_lieu]' => 0,
    '[conf_rence:field-lieu]' => 0,
    '[conf_rence:field-sujet-de-conference]' => 0,
    '[conf_rence:field_sujet_de_conference]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
