<?php
/**
 * @file
 * permission.eck_list_conf_rence_bundles.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck list conf_rence bundles',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
