<?php
/**
 * @file
 * permission.eck_view_conf_rence_bundles.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck view conf_rence bundles',
  'roles' => array(
    0 => 'administrator',
    1 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
