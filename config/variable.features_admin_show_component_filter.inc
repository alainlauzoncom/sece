<?php
/**
 * @file
 * variable.features_admin_show_component_filter.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_show_component_filter',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
