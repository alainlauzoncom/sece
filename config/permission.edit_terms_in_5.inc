<?php
/**
 * @file
 * permission.edit_terms_in_5.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit terms in 5',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
