<?php
/**
 * @file
 * permission.use_workbench_moderation_needs_review_tab.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'use workbench_moderation needs review tab',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
