<?php
/**
 * @file
 * text_format.full_html.inc
 */

$api = '2.0.0';

$data = (object) array(
  'format' => 'full_html',
  'name' => 'Full HTML',
  'cache' => '0',
  'status' => '1',
  'weight' => '1',
  'filters' => array(
    'filter_tokens' => array(
      'weight' => '-48',
      'status' => '1',
      'settings' => array(),
    ),
    'filter_url' => array(
      'weight' => '-47',
      'status' => '1',
      'settings' => array(
        'filter_url_length' => '72',
      ),
    ),
    'filter_autop' => array(
      'weight' => '-46',
      'status' => '1',
      'settings' => array(),
    ),
    'filter_htmlcorrector' => array(
      'weight' => '-44',
      'status' => '1',
      'settings' => array(),
    ),
  ),
);

$dependencies = array();

$optional = array(
  'permission.use_text_format_full_html' => 'permission.use_text_format_full_html',
  'wysiwyg.full_html' => 'wysiwyg.full_html',
);

$modules = array(
  0 => 'token_filter',
  1 => 'filter',
);
