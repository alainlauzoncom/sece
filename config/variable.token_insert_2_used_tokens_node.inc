<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_node.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_node',
  'content' => array(
    '[node:author]' => 0,
    '[node:field-categories]' => 0,
    '[node:field_categories]' => 0,
    '[node:body]' => 0,
    '[node:revision]' => 0,
    '[node:created]' => 0,
    '[node:changed]' => 0,
    '[node:field_donnee]' => 0,
    '[node:field-donnee]' => 0,
    '[node:vid]' => 0,
    '[node:nid]' => 0,
    '[node:is-new]' => 0,
    '[node:language]' => 0,
    '[node:menu-link]' => 0,
    '[node:log]' => 0,
    '[node:edit-url]' => 0,
    '[node:field_numero_sequentiel]' => 0,
    '[node:field-numero-sequentiel]' => 0,
    '[node:source]' => 0,
    '[node:promote]' => 0,
    '[node:summary]' => 0,
    '[node:status]' => 0,
    '[node:sticky]' => 0,
    '[node:field_thematiques_d_intervention]' => 0,
    '[node:field-thematiques-d-intervention]' => 0,
    '[node:title]' => 0,
    '[node:field-type]' => 0,
    '[node:field_type]' => 0,
    '[node:content-type]' => 0,
    '[node:url]' => 0,
    '[node:original]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
