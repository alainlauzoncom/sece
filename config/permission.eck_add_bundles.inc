<?php
/**
 * @file
 * permission.eck_add_bundles.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck add bundles',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
