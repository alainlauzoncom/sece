<?php
/**
 * @file
 * permission.manage_sece_nombre_de_sujets_par_pair_properties.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'manage sece_nombre_de_sujets_par_pair properties',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
