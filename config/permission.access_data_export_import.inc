<?php
/**
 * @file
 * permission.access_data_export_import.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'access data export import',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'data_export_import',
);
