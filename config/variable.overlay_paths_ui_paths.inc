<?php
/**
 * @file
 * variable.overlay_paths_ui_paths.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'overlay_paths_ui_paths',
  'content' => array(
    'sece_overlay/*' => TRUE,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
