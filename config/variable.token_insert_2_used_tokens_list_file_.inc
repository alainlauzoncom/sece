<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_list_file_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_list<file>',
  'content' => array(
    '[list<file>:0]' => 0,
    '[list<file>:1]' => 0,
    '[list<file>:2]' => 0,
    '[list<file>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
