<?php
/**
 * @file
 * field.user.field_nom_complet.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_nom_complet' => array(
              'value' => 'field_nom_complet_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_nom_complet' => array(
              'value' => 'field_nom_complet_value',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'field_name' => 'field_nom_complet',
    'type' => 'concat_field',
    'module' => 'concat_field',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Nom complet',
    'widget' => array(
      'weight' => '13',
      'type' => 'concat_field_default',
      'module' => 'concat_field',
      'active' => 0,
      'settings' => array(),
    ),
    'settings' => array(
      'selected_fields' => array(
        'field_prenom' => 'field_prenom',
        'field_nom' => 'field_nom',
        'concat_field_entity_label' => 0,
        'field_numero_de_telephone' => 0,
        'field_statut' => 0,
        'field_site_web' => 0,
      ),
      'user_register_form' => 1,
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'hidden',
        'weight' => '13',
        'settings' => array(),
      ),
    ),
    'required' => 1,
    'description' => '',
    'field_name' => 'field_nom_complet',
    'entity_type' => 'user',
    'bundle' => 'user',
    'deleted' => '0',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'concat_field',
);
