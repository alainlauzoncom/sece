<?php
/**
 * @file
 * variable.menu_default_active_menus.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'menu_default_active_menus',
  'content' => array(
    0 => 'devel',
    1 => 'features',
    2 => 'management',
    3 => 'menu-menu-de-gauche',
    4 => 'main-menu',
    5 => 'navigation',
    7 => 'user-menu',
    8 => 'menu-sous-menu-usager-a-recevoir',
    9 => 'menu-sous-menu-usager-a-donner',
    10 => 'menu-sous-menu-usager---proposit',
    11 => 'menu-sous-menu-usager---mes-rens',
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
