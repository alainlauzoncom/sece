<?php
/**
 * @file
 * permission.eck_list_entities.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck list entities',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
