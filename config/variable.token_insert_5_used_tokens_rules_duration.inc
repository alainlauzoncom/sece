<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_rules_duration.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_rules_duration',
  'content' => array(
    '[rules_duration:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
