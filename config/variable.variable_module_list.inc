<?php
/**
 * @file
 * variable.variable_module_list.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'variable_module_list',
  'content' => array(
    'i18n' => array(
      0 => 'i18n_language_list',
    ),
    'environment_indicator' => array(
      0 => 'environment_indicator_overwrite',
      1 => 'environment_indicator_overwritten_color',
      2 => 'environment_indicator_overwritten_text_color',
      3 => 'environment_indicator_overwritten_fixed',
      4 => 'environment_indicator_overwritten_position',
      5 => 'environment_indicator_overwritten_name',
      6 => 'environment_indicator_suppress_pages',
    ),
    'autoassignrole' => array(
      0 => 'autoassignrole_user_fieldset_title',
      1 => 'autoassignrole_user_title',
      2 => 'autoassignrole_user_description',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
