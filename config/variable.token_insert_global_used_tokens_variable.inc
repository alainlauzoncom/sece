<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_variable.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_variable',
  'content' => array(
    '[variable:site_mail]' => 0,
    '[variable:feed_description]' => 0,
    '[variable:user_picture_dimensions]' => 0,
    '[variable:user_picture_guidelines]' => 0,
    '[variable:user_registration_help]' => 0,
    '[variable:maintenance_mode_message]' => 0,
    '[variable:site_name]' => 0,
    '[variable:user_picture_default]' => 0,
    '[variable:site_slogan]' => 0,
    '[variable:user_picture_file_size]' => 0,
    '[variable:environment_indicator_suppress_pages]' => 0,
    '[variable:anonymous]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
