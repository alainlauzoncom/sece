<?php
/**
 * @file
 * variable.dexp_menudexp_menu_block_1dexp_block_responsive.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'dexp_menudexp_menu_block_1dexp_block_responsive',
  'content' => array(
    'hphone' => 0,
    'vphone' => 0,
    'htablet' => 0,
    'vtablet' => 0,
    'hdesktop' => 0,
    'vdesktop' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
