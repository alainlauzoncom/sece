<?php
/**
 * @file
 * variable.maxlength_js_label_conf_rence.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'maxlength_js_label_conf_rence',
  'content' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
);

$dependencies = array();

$optional = array();

$modules = array();
