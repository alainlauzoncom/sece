<?php
/**
 * @file
 * variable.views_block_hashes.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'views_block_hashes',
  'content' => array(
    '4b40d27bbaacd1d170bd653606b5183b' => '-exp-sujets_de_conference-page_2',
    '5b9e0eeaba2a9545ca5af0e88ce663a7' => '-exp-sujets_de_conference-page_4',
    'e718a3d7e41a649d18a1608e07bb32d8' => '-exp-sujets_de_conference-page_1',
    '9cf22433e0efbd8b206c8e4c2edb177c' => 'workbench_recent_content-block_1',
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
