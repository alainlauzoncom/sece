<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_rules_decimal.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_rules_decimal',
  'content' => array(
    '[rules_decimal:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
