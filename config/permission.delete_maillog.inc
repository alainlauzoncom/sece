<?php
/**
 * @file
 * permission.delete_maillog.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete maillog',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'maillog',
);
