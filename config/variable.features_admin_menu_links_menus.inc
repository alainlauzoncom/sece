<?php
/**
 * @file
 * variable.features_admin_menu_links_menus.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_menu_links_menus',
  'content' => array(
    'devel' => 'devel',
    'features' => 'features',
    'management' => 'management',
    'menu-menu-de-gauche' => 'menu-menu-de-gauche',
    'main-menu' => 'main-menu',
    'navigation' => 'navigation',
    'user-menu' => 'user-menu',
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
