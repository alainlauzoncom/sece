<?php
/**
 * @file
 * wysiwyg.plain_text.inc
 */

$api = '2.0.0';

$data = FALSE;

$dependencies = array(
  'text_format.plain_text' => 'text_format.plain_text',
);

$optional = array();

$modules = array(
  0 => 'wysiwyg',
);
