<?php
/**
 * @file
 * views_view.admin_views_user.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'admin_views_user';
$view->description = 'List, add, and edit users.';
$view->tag = 'admin';
$view->base_table = 'users';
$view->human_name = 'Administration: Users';
$view->core = 0;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['title'] = 'Utilisateurs';
$handler->display->display_options['css_class'] = 'admin-views-view';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'plus';
$handler->display->display_options['access']['type'] = 'menu';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
$handler->display->display_options['pager']['type'] = 'infinite_scroll';
$handler->display->display_options['pager']['options']['items_per_page'] = '0';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'views_calc_distinct';
$handler->display->display_options['style_options']['columns'] = array(
  'name' => 'name',
  'mail' => 'mail',
  'status' => 'status',
  'rid' => 'rid',
  'created' => 'created',
  'access' => 'access',
  'field__tablissement' => 'field__tablissement',
  'field_statut' => 'field_statut',
  'field_nombre_de_conferences_prop' => 'field_nombre_de_conferences_prop',
  'field_nombre_de_conferences_donn' => 'field_nombre_de_conferences_donn',
  'field_nombre_de_conferences_recu' => 'field_nombre_de_conferences_recu',
  'field_nombre_de_prises_de_contac' => 'field_nombre_de_prises_de_contac',
  'uid_1' => 'uid_1',
  'edit_node' => 'edit_node',
  'cancel_node' => 'cancel_node',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'mail' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'rid' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'access' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'field__tablissement' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'field_statut' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'field_nombre_de_conferences_prop' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 1,
    'calc' => array(
      'SUM' => 'SUM',
      'AVG' => 'AVG',
    ),
  ),
  'field_nombre_de_conferences_donn' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 1,
    'calc' => array(
      'SUM' => 'SUM',
      'AVG' => 'AVG',
    ),
  ),
  'field_nombre_de_conferences_recu' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 1,
    'calc' => array(
      'SUM' => 'SUM',
      'AVG' => 'AVG',
    ),
  ),
  'field_nombre_de_prises_de_contac' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 1,
    'calc' => array(
      'SUM' => 'SUM',
      'AVG' => 'AVG',
    ),
  ),
  'uid_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(
      'COUNT' => 'COUNT',
      'SUM' => 'SUM',
      'AVG' => 'AVG',
      'MIN' => 'MIN',
    ),
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'cancel_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
$handler->display->display_options['style_options']['detailed_values'] = '0';
$handler->display->display_options['style_options']['precision'] = '2';
$handler->display->display_options['style_options']['views_calc_distinct'] = 1;
/* Entête: Gestionnaire défectueux/manquant */
$handler->display->display_options['header']['webform_result']['id'] = 'webform_result';
$handler->display->display_options['header']['webform_result']['table'] = 'views';
$handler->display->display_options['header']['webform_result']['field'] = 'webform_result';
$handler->display->display_options['header']['webform_result']['label'] = '';
/* Pied de page: Gestionnaire défectueux/manquant */
$handler->display->display_options['footer']['webform_result']['id'] = 'webform_result';
$handler->display->display_options['footer']['webform_result']['table'] = 'views';
$handler->display->display_options['footer']['webform_result']['field'] = 'webform_result';
/* Comportement en l'absence de résultats: Global : Texte non filtré */
$handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
$handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
$handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
$handler->display->display_options['empty']['area_text_custom']['content'] = 'No users available.';
/* Champ: Utilisateur : Nom */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
/* Champ: Utilisateur : Courriel */
$handler->display->display_options['fields']['mail']['id'] = 'mail';
$handler->display->display_options['fields']['mail']['table'] = 'users';
$handler->display->display_options['fields']['mail']['field'] = 'mail';
$handler->display->display_options['fields']['mail']['label'] = '';
$handler->display->display_options['fields']['mail']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['mail']['link_to_user'] = '0';
/* Champ: Utilisateur : Actif */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'users';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Champ: Utilisateur : Rôles */
$handler->display->display_options['fields']['rid']['id'] = 'rid';
$handler->display->display_options['fields']['rid']['table'] = 'users_roles';
$handler->display->display_options['fields']['rid']['field'] = 'rid';
$handler->display->display_options['fields']['rid']['type'] = 'ul';
/* Champ: Utilisateur : Date de création */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'users';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Membre depuis';
$handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
/* Champ: Utilisateur : Dernier accès */
$handler->display->display_options['fields']['access']['id'] = 'access';
$handler->display->display_options['fields']['access']['table'] = 'users';
$handler->display->display_options['fields']['access']['field'] = 'access';
$handler->display->display_options['fields']['access']['date_format'] = 'time ago';
/* Champ: Utilisateur : Établissement */
$handler->display->display_options['fields']['field__tablissement']['id'] = 'field__tablissement';
$handler->display->display_options['fields']['field__tablissement']['table'] = 'field_data_field__tablissement';
$handler->display->display_options['fields']['field__tablissement']['field'] = 'field__tablissement';
/* Champ: Utilisateur : Statut */
$handler->display->display_options['fields']['field_statut']['id'] = 'field_statut';
$handler->display->display_options['fields']['field_statut']['table'] = 'field_data_field_statut';
$handler->display->display_options['fields']['field_statut']['field'] = 'field_statut';
$handler->display->display_options['fields']['field_statut']['type'] = 'taxonomy_term_reference_plain';
/* Champ: Utilisateur : Nombre de conférences proposées */
$handler->display->display_options['fields']['field_nombre_de_conferences_prop']['id'] = 'field_nombre_de_conferences_prop';
$handler->display->display_options['fields']['field_nombre_de_conferences_prop']['table'] = 'field_data_field_nombre_de_conferences_prop';
$handler->display->display_options['fields']['field_nombre_de_conferences_prop']['field'] = 'field_nombre_de_conferences_prop';
$handler->display->display_options['fields']['field_nombre_de_conferences_prop']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Champ: Utilisateur : Nombre de conférences données */
$handler->display->display_options['fields']['field_nombre_de_conferences_donn']['id'] = 'field_nombre_de_conferences_donn';
$handler->display->display_options['fields']['field_nombre_de_conferences_donn']['table'] = 'field_data_field_nombre_de_conferences_donn';
$handler->display->display_options['fields']['field_nombre_de_conferences_donn']['field'] = 'field_nombre_de_conferences_donn';
$handler->display->display_options['fields']['field_nombre_de_conferences_donn']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Champ: Utilisateur : Nombre de conférences reçues */
$handler->display->display_options['fields']['field_nombre_de_conferences_recu']['id'] = 'field_nombre_de_conferences_recu';
$handler->display->display_options['fields']['field_nombre_de_conferences_recu']['table'] = 'field_data_field_nombre_de_conferences_recu';
$handler->display->display_options['fields']['field_nombre_de_conferences_recu']['field'] = 'field_nombre_de_conferences_recu';
$handler->display->display_options['fields']['field_nombre_de_conferences_recu']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Champ: Utilisateur : Nombre de prises de contact */
$handler->display->display_options['fields']['field_nombre_de_prises_de_contac']['id'] = 'field_nombre_de_prises_de_contac';
$handler->display->display_options['fields']['field_nombre_de_prises_de_contac']['table'] = 'field_data_field_nombre_de_prises_de_contac';
$handler->display->display_options['fields']['field_nombre_de_prises_de_contac']['field'] = 'field_nombre_de_prises_de_contac';
$handler->display->display_options['fields']['field_nombre_de_prises_de_contac']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Champ: Utilisateur : Lien de modification */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'users';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Actions';
/* Champ: Utilisateur : Lien d'annulation */
$handler->display->display_options['fields']['cancel_node']['id'] = 'cancel_node';
$handler->display->display_options['fields']['cancel_node']['table'] = 'users';
$handler->display->display_options['fields']['cancel_node']['field'] = 'cancel_node';
$handler->display->display_options['fields']['cancel_node']['label'] = '';
$handler->display->display_options['fields']['cancel_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['cancel_node']['hide_alter_empty'] = FALSE;
/* Critère de tri: Utilisateur : Date de création */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'users';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Critère de tri: Contenu : Type */
$handler->display->display_options['sorts']['type']['id'] = 'type';
$handler->display->display_options['sorts']['type']['table'] = 'node';
$handler->display->display_options['sorts']['type']['field'] = 'type';
$handler->display->display_options['sorts']['type']['relationship'] = 'uid';
/* Critère de filtrage: Utilisateur : Nom */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'users';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['operator'] = 'not in';
$handler->display->display_options['filters']['uid']['value'] = array(
  0 => 0,
);
$handler->display->display_options['filters']['uid']['group'] = 1;
/* Critère de filtrage: Utilisateur : Nom (brut) */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'users';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['operator'] = 'contains';
$handler->display->display_options['filters']['name']['group'] = 1;
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'Nom d\'utilisateur';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['remember'] = TRUE;
/* Critère de filtrage: Utilisateur : Courriel */
$handler->display->display_options['filters']['mail']['id'] = 'mail';
$handler->display->display_options['filters']['mail']['table'] = 'users';
$handler->display->display_options['filters']['mail']['field'] = 'mail';
$handler->display->display_options['filters']['mail']['operator'] = 'contains';
$handler->display->display_options['filters']['mail']['group'] = 1;
$handler->display->display_options['filters']['mail']['exposed'] = TRUE;
$handler->display->display_options['filters']['mail']['expose']['operator_id'] = 'mail_op';
$handler->display->display_options['filters']['mail']['expose']['label'] = 'Courriel';
$handler->display->display_options['filters']['mail']['expose']['operator'] = 'mail_op';
$handler->display->display_options['filters']['mail']['expose']['identifier'] = 'mail';
$handler->display->display_options['filters']['mail']['expose']['remember'] = TRUE;
/* Critère de filtrage: Utilisateur : Actif */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'users';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Actif';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember'] = TRUE;
/* Critère de filtrage: Utilisateur : Rôles */
$handler->display->display_options['filters']['rid']['id'] = 'rid';
$handler->display->display_options['filters']['rid']['table'] = 'users_roles';
$handler->display->display_options['filters']['rid']['field'] = 'rid';
$handler->display->display_options['filters']['rid']['group'] = 1;
$handler->display->display_options['filters']['rid']['exposed'] = TRUE;
$handler->display->display_options['filters']['rid']['expose']['operator_id'] = 'rid_op';
$handler->display->display_options['filters']['rid']['expose']['label'] = 'Rôle';
$handler->display->display_options['filters']['rid']['expose']['operator'] = 'rid_op';
$handler->display->display_options['filters']['rid']['expose']['identifier'] = 'rid';
$handler->display->display_options['filters']['rid']['expose']['remember'] = TRUE;
/* Critère de filtrage: Utilisateur : Établissement (field__tablissement) */
$handler->display->display_options['filters']['field__tablissement_value']['id'] = 'field__tablissement_value';
$handler->display->display_options['filters']['field__tablissement_value']['table'] = 'field_data_field__tablissement';
$handler->display->display_options['filters']['field__tablissement_value']['field'] = 'field__tablissement_value';
$handler->display->display_options['filters']['field__tablissement_value']['group'] = 1;
$handler->display->display_options['filters']['field__tablissement_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field__tablissement_value']['expose']['operator_id'] = 'field__tablissement_value_op';
$handler->display->display_options['filters']['field__tablissement_value']['expose']['label'] = 'Établissement';
$handler->display->display_options['filters']['field__tablissement_value']['expose']['operator'] = 'field__tablissement_value_op';
$handler->display->display_options['filters']['field__tablissement_value']['expose']['identifier'] = 'field__tablissement_value';
$handler->display->display_options['filters']['field__tablissement_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['field__tablissement_value']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field__tablissement_value']['expose']['remember_roles'] = array(
  2 => 0,
  3 => '3',
  4 => '4',
  1 => 0,
  5 => 0,
);
$handler->display->display_options['filters']['field__tablissement_value']['reduce_duplicates'] = TRUE;
/* Critère de filtrage: Utilisateur : Statut (field_statut) */
$handler->display->display_options['filters']['field_statut_tid']['id'] = 'field_statut_tid';
$handler->display->display_options['filters']['field_statut_tid']['table'] = 'field_data_field_statut';
$handler->display->display_options['filters']['field_statut_tid']['field'] = 'field_statut_tid';
$handler->display->display_options['filters']['field_statut_tid']['group'] = 1;
$handler->display->display_options['filters']['field_statut_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_statut_tid']['expose']['operator_id'] = 'field_statut_tid_op';
$handler->display->display_options['filters']['field_statut_tid']['expose']['label'] = 'Statut';
$handler->display->display_options['filters']['field_statut_tid']['expose']['operator'] = 'field_statut_tid_op';
$handler->display->display_options['filters']['field_statut_tid']['expose']['identifier'] = 'field_statut_tid';
$handler->display->display_options['filters']['field_statut_tid']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['field_statut_tid']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_statut_tid']['expose']['remember_roles'] = array(
  2 => 0,
  3 => '3',
  4 => '4',
  1 => 0,
  5 => 0,
);
$handler->display->display_options['filters']['field_statut_tid']['reduce_duplicates'] = TRUE;
$handler->display->display_options['filters']['field_statut_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_statut_tid']['vocabulary'] = 'statut';
/* Critère de filtrage: Utilisateur : Identifiant (ID) de l'utilisateur */
$handler->display->display_options['filters']['uid_raw']['id'] = 'uid_raw';
$handler->display->display_options['filters']['uid_raw']['table'] = 'users';
$handler->display->display_options['filters']['uid_raw']['field'] = 'uid_raw';
$handler->display->display_options['filters']['uid_raw']['group'] = 1;
$handler->display->display_options['filters']['uid_raw']['exposed'] = TRUE;
$handler->display->display_options['filters']['uid_raw']['expose']['operator_id'] = 'uid_raw_op';
$handler->display->display_options['filters']['uid_raw']['expose']['label'] = 'Identifiant (ID) de l\'utilisateur';
$handler->display->display_options['filters']['uid_raw']['expose']['operator'] = 'uid_raw_op';
$handler->display->display_options['filters']['uid_raw']['expose']['identifier'] = 'uid_raw';
$handler->display->display_options['filters']['uid_raw']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['uid_raw']['expose']['remember_roles'] = array(
  2 => 0,
  3 => '3',
  4 => '4',
  1 => 0,
  5 => 0,
);

/* Display: System */
$handler = $view->new_display('system', 'System', 'system_1');
$handler->display->display_options['defaults']['group_by'] = FALSE;
$handler->display->display_options['defaults']['access'] = FALSE;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Critère de tri: Utilisateur : Date de création */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'users';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
$handler->display->display_options['path'] = 'admin/people';
$translatables['admin_views_user'] = array(
  t('Defaults'),
  t('Utilisateurs'),
  t('plus'),
  t('Appliquer'),
  t('Réinitialiser'),
  t('Trier par'),
  t('Asc'),
  t('Desc'),
  t('Show More'),
  t('webform_result'),
  t('No users available.'),
  t('Nom'),
  t('Actif'),
  t('Rôles'),
  t('Membre depuis'),
  t('Dernier accès'),
  t('Établissement'),
  t('Statut'),
  t('Nombre de conférences proposées'),
  t('Nombre de conférences données'),
  t('Nombre de conférences reçues'),
  t('Nombre de prises de contact'),
  t('Actions'),
  t('Nom d\'utilisateur'),
  t('Courriel'),
  t('Rôle'),
  t('Identifiant (ID) de l\'utilisateur'),
  t('System'),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'user',
  2 => 'list',
  3 => 'taxonomy',
  4 => 'number',
);
