<?php
/**
 * @file
 * permission.administer_workbench_moderation.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer workbench moderation',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
