<?php
/**
 * @file
 * variable.logintoboggan_minimum_password_length.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'logintoboggan_minimum_password_length',
  'content' => '6',
);

$dependencies = array();

$optional = array();

$modules = array();
