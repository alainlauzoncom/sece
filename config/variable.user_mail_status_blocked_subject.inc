<?php
/**
 * @file
 * variable.user_mail_status_blocked_subject.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_status_blocked_subject',
  'content' => 'Détails du compte [user:name] sur [site:name] (bloqué)',
);

$dependencies = array();

$optional = array();

$modules = array();
