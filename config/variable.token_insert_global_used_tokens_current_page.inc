<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_current_page.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_current-page',
  'content' => array(
    '[current-page:page-number]' => 0,
    '[current-page:title]' => 0,
    '[current-page:url]' => 0,
    '[current-page:query]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
