<?php
/**
 * @file
 * variable.user_mail_password_reset_subject.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_password_reset_subject',
  'content' => 'Informations de changement de mot de passe pour [user:name-raw] sur [site:name]',
);

$dependencies = array();

$optional = array();

$modules = array();
