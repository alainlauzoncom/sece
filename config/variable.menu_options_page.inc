<?php
/**
 * @file
 * variable.menu_options_page.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'menu_options_page',
  'content' => array(
    0 => 'menu-menu-de-gauche',
    1 => 'main-menu',
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
