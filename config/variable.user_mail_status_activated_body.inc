<?php
/**
 * @file
 * variable.user_mail_status_activated_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_status_activated_body',
  'content' => '[user:name-raw],

Votre compte sur [site:name] a été activé.

Vous pouvez désormais vous identifier en cliquant sur ce lien ou en le copiant dans votre navigateur :

[user:one-time-login-url]

Ce lien ne peut être utilisé pour s\'identifier qu\'une seule fois et il vous conduira à une page où vous pourrez paramétrer votre mot de passe.

Après avoir paramétré votre mot de passe, vous pourrez vous identifier à l\'adresse [site:login-url] lors de vos prochaines connexions :

identifiant : [user:name-raw]
mot de passe : Votre mot de passe

--  L\'équipe [site:name]',
);

$dependencies = array();

$optional = array();

$modules = array();
