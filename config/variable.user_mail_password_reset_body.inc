<?php
/**
 * @file
 * variable.user_mail_password_reset_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_password_reset_body',
  'content' => 'Bonjour [user:field_nom_complet] [user:name-raw],

Une demande de renouvellement de mot de passe a été faite pour votre compte sur SECE.

Vous pouvez désormais vous identifier en cliquant sur ce lien ou en le copiant dans votre navigateur :

[user:one-time-login-url]

Ce lien ne peut être utilisé pour s\'identifier qu\'une seule fois et il vous conduira à une page où vous pourrez paramétrer votre mot de passe. Il expirera après un jour et rien ne se passera s\'il n\'est pas utilisé.

--  L\'équipe SECE',
);

$dependencies = array();

$optional = array();

$modules = array();
