<?php
/**
 * @file
 * variable.manager_email.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'manager_email',
  'content' => 'stephanie.jagou@instituteddec.org',
);

$dependencies = array();

$optional = array();

$modules = array();
