<?php
/**
 * @file
 * permission.administer_unit_tests.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer unit tests',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'simpletest',
);
