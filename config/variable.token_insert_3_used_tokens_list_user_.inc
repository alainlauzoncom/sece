<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_list_user_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_list<user>',
  'content' => array(
    '[list<user>:0]' => 0,
    '[list<user>:1]' => 0,
    '[list<user>:2]' => 0,
    '[list<user>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
