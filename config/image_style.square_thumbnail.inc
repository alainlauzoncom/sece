<?php
/**
 * @file
 * image_style.square_thumbnail.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'square_thumbnail',
  'label' => 'square_thumbnail',
  'effects' => array(
    0 => array(
      'label' => 'Mise à l’échelle et recadrage',
      'help' => 'La mise à l\'échelle et le recadrage maintiendront les proportions originales de l\'image puis recadreront la dimension la plus large. C\'est très utile pour créer des vignettes carrées sans étirer les images.',
      'effect callback' => 'image_scale_and_crop_effect',
      'dimensions callback' => 'image_resize_dimensions',
      'form callback' => 'image_resize_form',
      'summary theme' => 'image_resize_summary',
      'module' => 'image',
      'name' => 'image_scale_and_crop',
      'data' => array(
        'width' => 180,
        'height' => 180,
      ),
      'weight' => '0',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'image',
);
