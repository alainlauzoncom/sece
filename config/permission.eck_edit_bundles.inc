<?php
/**
 * @file
 * permission.eck_edit_bundles.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck edit bundles',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
