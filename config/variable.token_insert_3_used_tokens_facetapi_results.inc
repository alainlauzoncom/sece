<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_facetapi_results.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_facetapi_results',
  'content' => array(
    '[facetapi_results:search-path]' => 0,
    '[facetapi_results:offset]' => 0,
    '[facetapi_results:end-count]' => 0,
    '[facetapi_results:keys]' => 0,
    '[facetapi_results:page-number]' => 0,
    '[facetapi_results:page-limit]' => 0,
    '[facetapi_results:page-total]' => 0,
    '[facetapi_results:result-count]' => 0,
    '[facetapi_results:start-count]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
