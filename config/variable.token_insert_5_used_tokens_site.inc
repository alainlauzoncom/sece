<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_site.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_site',
  'content' => array(
    '[site:mail]' => 0,
    '[site:current-date]' => 0,
    '[site:current-user]' => 0,
    '[site:name]' => 0,
    '[site:current-page]' => 0,
    '[site:login-url]' => 0,
    '[site:slogan]' => 0,
    '[site:url]' => 0,
    '[site:url-brief]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
