<?php
/**
 * @file
 * permission.use_on_page_translation.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'use on-page translation',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'l10n_client',
);
