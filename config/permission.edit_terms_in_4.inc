<?php
/**
 * @file
 * permission.edit_terms_in_4.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit terms in 4',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
