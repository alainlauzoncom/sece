<?php
/**
 * @file
 * variable.features_admin_show_component_backup_migrate_sources.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_show_component_backup_migrate_sources',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
