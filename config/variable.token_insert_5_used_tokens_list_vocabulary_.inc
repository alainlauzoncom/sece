<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_list_vocabulary_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_list<vocabulary>',
  'content' => array(
    '[list<vocabulary>:0]' => 0,
    '[list<vocabulary>:1]' => 0,
    '[list<vocabulary>:2]' => 0,
    '[list<vocabulary>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
