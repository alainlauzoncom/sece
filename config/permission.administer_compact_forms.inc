<?php
/**
 * @file
 * permission.administer_compact_forms.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer Compact Forms',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'compact_forms',
);
