<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_vocabulary.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_vocabulary',
  'content' => array(
    '[vocabulary:node-count]' => 0,
    '[vocabulary:description]' => 0,
    '[vocabulary:vid]' => 0,
    '[vocabulary:edit-url]' => 0,
    '[vocabulary:name]' => 0,
    '[vocabulary:term-count]' => 0,
    '[vocabulary:machine-name]' => 0,
    '[vocabulary:original]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
