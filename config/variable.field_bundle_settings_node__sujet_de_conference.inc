<?php
/**
 * @file
 * variable.field_bundle_settings_node__sujet_de_conference.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'field_bundle_settings_node__sujet_de_conference',
  'content' => array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '8',
        ),
      ),
      'display' => array(),
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
