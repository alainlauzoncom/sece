<?php
/**
 * @file
 * image_style.thumbnail.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'thumbnail',
  'label' => 'Thumbnail (100x100)',
  'effects' => array(
    0 => array(
      'label' => 'Échelle',
      'help' => 'La mise à l\'échelle maintiendra les proportions originales de l\'image. Si une seule dimension est précisée, l\'autre dimension sera calculée automatiquement.',
      'effect callback' => 'image_scale_effect',
      'dimensions callback' => 'image_scale_dimensions',
      'form callback' => 'image_scale_form',
      'summary theme' => 'image_scale_summary',
      'module' => 'image',
      'name' => 'image_scale',
      'data' => array(
        'width' => 100,
        'height' => 100,
        'upscale' => 1,
      ),
      'weight' => '0',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'image',
);
