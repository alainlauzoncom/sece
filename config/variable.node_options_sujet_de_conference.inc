<?php
/**
 * @file
 * variable.node_options_sujet_de_conference.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'node_options_sujet_de_conference',
  'content' => array(
    0 => 'moderation',
    1 => 'revision',
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
