<?php
/**
 * @file
 * field.user.field_site_web.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_site_web' => array(
              'value' => 'field_site_web_value',
              'title' => 'field_site_web_title',
              'attributes' => 'field_site_web_attributes',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_site_web' => array(
              'value' => 'field_site_web_value',
              'title' => 'field_site_web_title',
              'attributes' => 'field_site_web_attributes',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'field_name' => 'field_site_web',
    'type' => 'url',
    'module' => 'url',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'description' => 'The URL string.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'title' => array(
        'description' => 'The title of the URL.',
        'type' => 'varchar',
        'length' => 1024,
        'not null' => FALSE,
      ),
      'attributes' => array(
        'description' => 'The serialized array of attributes of the URL.',
        'type' => 'blob',
        'size' => 'big',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Site Internet',
    'widget' => array(
      'weight' => '6',
      'type' => 'url_external',
      'module' => 'url',
      'active' => 1,
      'settings' => array(
        'size' => '80',
      ),
    ),
    'settings' => array(
      'title_field' => 0,
      'title_fetch' => 1,
      'user_register_form' => 1,
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'url_default',
        'weight' => '3',
        'settings' => array(
          'trim_length' => 80,
          'nofollow' => FALSE,
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'url',
      ),
    ),
    'required' => 0,
    'description' => '',
    'default_value' => NULL,
    'field_name' => 'field_site_web',
    'entity_type' => 'user',
    'bundle' => 'user',
    'deleted' => '0',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'url',
);
