<?php
/**
 * @file
 * permission.bypass_maxlength.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'bypass maxlength',
  'roles' => array(),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'maxlength',
);
