<?php
/**
 * @file
 * variable.user_mail_cancel_confirm_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_cancel_confirm_body',
  'content' => '[user:name-raw],

Une demande d\'annulation de votre compte a été faite sur [site:name].

Vous pouvez maintenant annuler votre compte sur [site:url-brief] en cliquant sur ce lien ou en le copiant dans votre navigateur :

[user:cancel-url]

REMARQUE : L\'annulation de votre compte n\'est pas reversible.

Ce lien expirera après un jour et rien ne se passera s\'il n\'est pas utilisé.

--  L\'équipe [site:name]',
);

$dependencies = array();

$optional = array();

$modules = array();
