<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_url.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_url',
  'content' => array(
    '[url:args]' => 0,
    '[url:path]' => 0,
    '[url:brief]' => 0,
    '[url:relative]' => 0,
    '[url:unaliased]' => 0,
    '[url:absolute]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
