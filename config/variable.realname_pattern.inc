<?php
/**
 * @file
 * variable.realname_pattern.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'realname_pattern',
  'content' => '[user:field-prenom] [user:field-nom]',
);

$dependencies = array();

$optional = array();

$modules = array();
