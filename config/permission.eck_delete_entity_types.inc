<?php
/**
 * @file
 * permission.eck_delete_entity_types.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck delete entity types',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
