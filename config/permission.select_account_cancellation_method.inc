<?php
/**
 * @file
 * permission.select_account_cancellation_method.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'select account cancellation method',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'user',
);
