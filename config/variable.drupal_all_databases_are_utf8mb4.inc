<?php
/**
 * @file
 * variable.drupal_all_databases_are_utf8mb4.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'drupal_all_databases_are_utf8mb4',
  'content' => TRUE,
);

$dependencies = array();

$optional = array();

$modules = array();
