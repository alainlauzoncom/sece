<?php
/**
 * @file
 * variable.features_admin_show_component_taxonomy.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_show_component_taxonomy',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
