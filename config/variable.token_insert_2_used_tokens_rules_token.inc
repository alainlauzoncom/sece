<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_rules_token.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_rules_token',
  'content' => array(
    '[rules_token:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
