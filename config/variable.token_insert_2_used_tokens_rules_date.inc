<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_rules_date.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_rules_date',
  'content' => array(
    '[rules_date:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
