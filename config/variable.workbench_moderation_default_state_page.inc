<?php
/**
 * @file
 * variable.workbench_moderation_default_state_page.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'workbench_moderation_default_state_page',
  'content' => 'draft',
);

$dependencies = array();

$optional = array();

$modules = array();
