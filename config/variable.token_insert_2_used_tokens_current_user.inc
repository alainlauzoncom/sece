<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_current_user.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_current-user',
  'content' => array(
    '[current-user:ip-address]' => 0,
    '[current-user:field_br_ve_presentation_de_l_ex]' => 0,
    '[current-user:field-br-ve-presentation-de-l-ex]' => 0,
    '[current-user:mail]' => 0,
    '[current-user:created]' => 0,
    '[current-user:last-access]' => 0,
    '[current-user:last-login]' => 0,
    '[current-user:field_faculte_departement]' => 0,
    '[current-user:field-faculte-departement]' => 0,
    '[current-user:uid]' => 0,
    '[current-user:edit-url]' => 0,
    '[current-user:field_nom]' => 0,
    '[current-user:field-nom]' => 0,
    '[current-user:field_nom_complet]' => 0,
    '[current-user:field-numero-de-telephone]' => 0,
    '[current-user:field_numero_de_telephone]' => 0,
    '[current-user:field_prenom]' => 0,
    '[current-user:field-prenom]' => 0,
    '[current-user:name-raw]' => 0,
    '[current-user:name]' => 0,
    '[current-user:roles]' => 0,
    '[current-user:field_site_web]' => 0,
    '[current-user:field-statut]' => 0,
    '[current-user:field_statut]' => 0,
    '[current-user:status]' => 0,
    '[current-user:field_sujets_de_conferences_pres]' => 0,
    '[current-user:field-sujets-de-conferences-pres]' => 0,
    '[current-user:theme]' => 0,
    '[current-user:url]' => 0,
    '[current-user:cancel-url]' => 0,
    '[current-user:one-time-login-url]' => 0,
    '[current-user:validate-url]' => 0,
    '[current-user:original]' => 0,
    '[current-user:field__tablissement]' => 0,
    '[current-user:field--tablissement]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
