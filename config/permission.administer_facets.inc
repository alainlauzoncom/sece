<?php
/**
 * @file
 * permission.administer_facets.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer facets',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'facetapi',
);
