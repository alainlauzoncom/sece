<?php
/**
 * @file
 * variable.views_exp_sujets_de_conference_pagedexp_animate.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'views-exp-sujets_de_conference-pagedexp_animate',
  'content' => 'fadeInDownBig',
);

$dependencies = array();

$optional = array();

$modules = array();
