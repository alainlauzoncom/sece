<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_menu_link.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_menu-link',
  'content' => array(
    '[menu-link:mlid]' => 0,
    '[menu-link:menu]' => 0,
    '[menu-link:edit-url]' => 0,
    '[menu-link:parent]' => 0,
    '[menu-link:parents]' => 0,
    '[menu-link:root]' => 0,
    '[menu-link:title]' => 0,
    '[menu-link:url]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
