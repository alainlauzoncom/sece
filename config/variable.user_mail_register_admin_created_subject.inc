<?php
/**
 * @file
 * variable.user_mail_register_admin_created_subject.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_register_admin_created_subject',
  'content' => 'Un administrateur a créé un compte pour vous sur le Système d\'échange de conférences en EDDEC (SECE)',
);

$dependencies = array();

$optional = array();

$modules = array();
