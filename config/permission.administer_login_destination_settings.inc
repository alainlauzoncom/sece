<?php
/**
 * @file
 * permission.administer_login_destination_settings.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer login destination settings',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'login_destination',
);
