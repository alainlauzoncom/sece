<?php
/**
 * @file
 * permission.administer_media.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer media',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'media',
);
