<?php
/**
 * @file
 * text_format.ds_code.inc
 */

$api = '2.0.0';

$data = (object) array(
  'format' => 'ds_code',
  'name' => 'Display Suite code',
  'cache' => '1',
  'status' => '1',
  'weight' => '12',
  'filters' => array(),
);

$dependencies = array();

$optional = array(
  'permission.use_text_format_ds_code' => 'permission.use_text_format_ds_code',
  'wysiwyg.ds_code' => 'wysiwyg.ds_code',
);

$modules = array();
