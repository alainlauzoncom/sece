<?php
/**
 * @file
 * field.node.field_ncd_sujet_de_conference.sujet_de_conference.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_ncd_sujet_de_conference' => array(
              'vname' => 'field_ncd_sujet_de_conference_vname',
              'vargs' => 'field_ncd_sujet_de_conference_vargs',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_ncd_sujet_de_conference' => array(
              'vname' => 'field_ncd_sujet_de_conference_vname',
              'vargs' => 'field_ncd_sujet_de_conference_vargs',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'field_name' => 'field_ncd_sujet_de_conference',
    'type' => 'viewfield',
    'module' => 'viewfield',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'vname' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
      ),
      'vargs' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Nombre de conférences données',
    'widget' => array(
      'weight' => '9',
      'type' => 'viewfield_select',
      'module' => 'viewfield',
      'active' => 0,
      'settings' => array(),
    ),
    'settings' => array(
      'force_default' => 1,
      'allowed_views' => array(
        'conferences' => 'conferences',
        'sujets_de_conference' => 0,
        'admin_views_node' => 0,
        'workbench_moderation' => 0,
        'admin_views_user' => 0,
        'webform_analysis' => 0,
        'webform_results' => 0,
        'webform_submissions' => 0,
        'webform_webforms' => 0,
        'workbench_current_user' => 0,
        'workbench_edited' => 0,
        'workbench_recent_content' => 0,
      ),
      'description_display' => 'after',
      'user_register_form' => FALSE,
    ),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'viewfield_default',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'viewfield',
        'weight' => 8,
      ),
      'full' => array(
        'type' => 'hidden',
        'label' => 'above',
        'settings' => array(),
        'weight' => 0,
      ),
      'teaser' => array(
        'type' => 'hidden',
        'label' => 'above',
        'settings' => array(),
        'weight' => 0,
      ),
    ),
    'required' => 0,
    'description' => '',
    'default_value' => array(
      0 => array(
        'vname' => 'conferences|page_4',
        'vargs' => '[node:nid]',
      ),
    ),
    'field_name' => 'field_ncd_sujet_de_conference',
    'entity_type' => 'node',
    'bundle' => 'sujet_de_conference',
    'deleted' => '0',
  ),
);

$dependencies = array(
  'content_type.sujet_de_conference' => 'content_type.sujet_de_conference',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'viewfield',
);
