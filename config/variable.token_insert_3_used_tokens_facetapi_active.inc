<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_facetapi_active.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_facetapi_active',
  'content' => array(
    '[facetapi_active:facet-label]' => 0,
    '[facetapi_active:active-value]' => 0,
    '[facetapi_active:facet-name]' => 0,
    '[facetapi_active:active-pos]' => 0,
    '[facetapi_active:active-value-raw]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
