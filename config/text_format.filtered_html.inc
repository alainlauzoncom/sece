<?php
/**
 * @file
 * text_format.filtered_html.inc
 */

$api = '2.0.0';

$data = (object) array(
  'format' => 'filtered_html',
  'name' => 'Filtered HTML',
  'cache' => '0',
  'status' => '1',
  'weight' => '0',
  'filters' => array(
    'filter_tokens' => array(
      'weight' => '-47',
      'status' => '1',
      'settings' => array(),
    ),
  ),
);

$dependencies = array();

$optional = array(
  'permission.use_text_format_filtered_html' => 'permission.use_text_format_filtered_html',
  'wysiwyg.filtered_html' => 'wysiwyg.filtered_html',
);

$modules = array(
  0 => 'token_filter',
);
