<?php
/**
 * @file
 * permission.edit_media.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'edit media',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'media',
);
