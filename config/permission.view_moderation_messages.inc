<?php
/**
 * @file
 * permission.view_moderation_messages.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'view moderation messages',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
    2 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
