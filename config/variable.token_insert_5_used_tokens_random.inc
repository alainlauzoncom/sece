<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_random.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_random',
  'content' => array(
    '[random:hash]' => 0,
    '[random:number]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
