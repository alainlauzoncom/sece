<?php
/**
 * @file
 * field.user.field__tablissement.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(
      'allowed_values' => array(
        'HEC Montréal' => 'HEC Montréal',
        'Polytechnique Montréal' => 'Polytechnique Montréal',
        'Université de Montréal' => 'Université de Montréal',
      ),
      'allowed_values_function' => '',
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field__tablissement' => array(
              'value' => 'field__tablissement_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field__tablissement' => array(
              'value' => 'field__tablissement_value',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'field_name' => 'field__tablissement',
    'type' => 'list_text',
    'module' => 'list',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Établissement',
    'widget' => array(
      'weight' => '9',
      'type' => 'options_select',
      'module' => 'options',
      'active' => 1,
      'settings' => array(),
    ),
    'settings' => array(
      'user_register_form' => 1,
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => '6',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'list',
      ),
    ),
    'required' => 1,
    'description' => '',
    'default_value' => NULL,
    'field_name' => 'field__tablissement',
    'entity_type' => 'user',
    'bundle' => 'user',
    'deleted' => '0',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'list',
  2 => 'options',
);
