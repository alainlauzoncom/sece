<?php
/**
 * @file
 * variable.entityreference_base_tables.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'entityreference:base-tables',
  'content' => array(
    'node' => array(
      0 => 'node',
      1 => 'nid',
    ),
    'taxonomy_term' => array(
      0 => 'taxonomy_term_data',
      1 => 'tid',
    ),
    'conf_rence' => array(
      0 => 'eck_conf_rence',
      1 => 'id',
    ),
    'sece_nombre_de_sujets_par_pair' => array(
      0 => 'eck_sece_nombre_de_sujets_par_pair',
      1 => 'id',
    ),
    'multifield' => array(
      0 => 'multifield',
      1 => 'id',
    ),
    'file' => array(
      0 => 'file_managed',
      1 => 'fid',
    ),
    'taxonomy_vocabulary' => array(
      0 => 'taxonomy_vocabulary',
      1 => 'vid',
    ),
    'user' => array(
      0 => 'users',
      1 => 'uid',
    ),
    'wysiwyg_profile' => array(
      0 => 'wysiwyg',
      1 => 'format',
    ),
    'workbench_moderation_transition' => array(
      0 => 'workbench_moderation_transitions',
      1 => 'id',
    ),
    'rules_config' => array(
      0 => 'rules_config',
      1 => 'id',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
