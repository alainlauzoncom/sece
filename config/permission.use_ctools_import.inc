<?php
/**
 * @file
 * permission.use_ctools_import.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'use ctools import',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
);
