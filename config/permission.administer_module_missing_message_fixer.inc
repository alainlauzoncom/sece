<?php
/**
 * @file
 * permission.administer_module_missing_message_fixer.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer module missing message fixer',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'module_missing_message_fixer',
);
