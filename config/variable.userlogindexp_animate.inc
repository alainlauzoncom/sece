<?php
/**
 * @file
 * variable.userlogindexp_animate.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'userlogindexp_animate',
  'content' => 'fadeInDownBig',
);

$dependencies = array();

$optional = array();

$modules = array();
