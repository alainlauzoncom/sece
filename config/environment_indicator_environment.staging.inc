<?php
/**
 * @file
 * environment_indicator_environment.staging.inc
 */

$api = '2.0.0';

$data = $environment = new stdClass();
$environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
$environment->api_version = 1;
$environment->machine = 'staging';
$environment->name = 'STAGING';
$environment->regexurl = 'staging.instituteddec-sece.org';
$environment->settings = array(
  'color' => '#2e1099',
  'text_color' => '#ffffff',
  'weight' => '',
  'position' => 'top',
  'fixed' => 1,
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'environment_indicator',
);
