<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_submission.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_submission',
  'content' => array(
    '[submission:access-token]' => 0,
    '[submission:ip-address]' => 0,
    '[submission:completed_date]' => 0,
    '[submission:modified_date]' => 0,
    '[submission:date]' => 0,
    '[submission:edit-url]' => 0,
    '[submission:serial]' => 0,
    '[submission:sid]' => 0,
    '[submission:user]' => 0,
    '[submission:url]' => 0,
    '[submission:values]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
