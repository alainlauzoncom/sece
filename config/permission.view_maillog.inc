<?php
/**
 * @file
 * permission.view_maillog.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'view maillog',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'maillog',
);
