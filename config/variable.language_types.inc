<?php
/**
 * @file
 * variable.language_types.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'language_types',
  'content' => array(
    'language' => TRUE,
    'language_content' => FALSE,
    'language_url' => FALSE,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
