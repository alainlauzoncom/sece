<?php
/**
 * @file
 * permission.moderate_content_from_needs_review_to_retired.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'moderate content from needs_review to retired',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
    2 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
