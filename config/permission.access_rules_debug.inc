<?php
/**
 * @file
 * permission.access_rules_debug.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'access rules debug',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'rules',
);
