<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_file.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_file',
  'content' => array(
    '[file:path]' => 0,
    '[file:extension]' => 0,
    '[file:timestamp]' => 0,
    '[file:fid]' => 0,
    '[file:basename]' => 0,
    '[file:name]' => 0,
    '[file:owner]' => 0,
    '[file:size]' => 0,
    '[file:size-raw]' => 0,
    '[file:mime]' => 0,
    '[file:url]' => 0,
    '[file:original]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
