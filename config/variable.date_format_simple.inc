<?php
/**
 * @file
 * variable.date_format_simple.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'date_format_simple',
  'content' => 'd/m/Y - H:i',
);

$dependencies = array();

$optional = array();

$modules = array();
