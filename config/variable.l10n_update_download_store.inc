<?php
/**
 * @file
 * variable.l10n_update_download_store.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'l10n_update_download_store',
  'content' => 'sites/all/translations',
);

$dependencies = array();

$optional = array();

$modules = array();
