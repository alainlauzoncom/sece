<?php
/**
 * @file
 * permission.administer_autoassignrole.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer autoassignrole',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'autoassignrole',
);
