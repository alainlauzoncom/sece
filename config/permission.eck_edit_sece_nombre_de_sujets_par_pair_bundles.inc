<?php
/**
 * @file
 * permission.eck_edit_sece_nombre_de_sujets_par_pair_bundles.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck edit sece_nombre_de_sujets_par_pair bundles',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
