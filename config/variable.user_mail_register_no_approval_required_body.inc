<?php
/**
 * @file
 * variable.user_mail_register_no_approval_required_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_register_no_approval_required_body',
  'content' => 'Bonjour [user:field_prenom] [user:field_nom],

Merci de vous être inscrit(e) au système d’échange de conférences en EDDEC (SECE). Votre inscription a été validée par l’Institut EDDEC.

Pour un accès complet au site, vous devez cliquer sur ce lien ou bien le copier/coller dans la barre d\'adresse de votre navigateur.

[user:validate-url]

Si vous n’avez pas inscrit de demande ou d’offre de conférence, l’Institut EDDEC vous invite à venir bonifier votre fiche d’inscription au cours des 10 prochains jours afin d’assurer le dynamisme et le bon fonctionnement du SECE. 

Pour toute question relative au SECE et à son utilisation, n’hésitez pas à communiquer avec l’Institut EDDEC.

Bons échanges!

Stéphanie Jagou
Chargée de projets en développement durable et communications
stephanie.jagou@instituteddec.org
Tél. : +1 514 340.4711, poste 2150
Institut EDDEC – environnement, développement durable, économie circulaire
Université de Montréal, HEC Montréal, Polytechnique Montréal
www.instituteddec.org',
);

$dependencies = array();

$optional = array();

$modules = array();
