<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_list_date_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_list<date>',
  'content' => array(
    '[list<date>:0]' => 0,
    '[list<date>:1]' => 0,
    '[list<date>:2]' => 0,
    '[list<date>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
