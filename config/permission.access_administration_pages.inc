<?php
/**
 * @file
 * permission.access_administration_pages.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'access administration pages',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'system',
);
