<?php
/**
 * @file
 * permission.eck_delete_conf_rence_conf_rence_entities.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck delete conf_rence conf_rence entities',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
