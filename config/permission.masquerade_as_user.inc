<?php
/**
 * @file
 * permission.masquerade_as_user.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'masquerade as user',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'masquerade',
);
