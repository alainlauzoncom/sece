<?php
/**
 * @file
 * variable.features_rebuild_on_flush.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_rebuild_on_flush',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
