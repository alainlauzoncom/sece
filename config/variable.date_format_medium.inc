<?php
/**
 * @file
 * variable.date_format_medium.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'date_format_medium',
  'content' => 'd/m/Y G:H',
);

$dependencies = array();

$optional = array();

$modules = array();
