<?php
/**
 * @file
 * variable.viewse718a3d7e41a649d18a1608e07bb32d8dexp_block_responsive.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'viewse718a3d7e41a649d18a1608e07bb32d8dexp_block_responsive',
  'content' => array(
    'hphone' => 0,
    'vphone' => 0,
    'htablet' => 0,
    'vtablet' => 0,
    'hdesktop' => 0,
    'vdesktop' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
