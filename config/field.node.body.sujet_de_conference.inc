<?php
/**
 * @file
 * field.node.body.sujet_de_conference.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'entity_types' => array(
      0 => 'node',
    ),
    'translatable' => '1',
    'settings' => array(),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_body' => array(
              'value' => 'body_value',
              'summary' => 'body_summary',
              'format' => 'body_format',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_body' => array(
              'value' => 'body_value',
              'summary' => 'body_summary',
              'format' => 'body_format',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(
      'format' => array(
        'table' => 'filter_format',
        'columns' => array(
          'format' => 'format',
        ),
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'field_name' => 'body',
    'type' => 'text_with_summary',
    'module' => 'text',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'summary' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Description',
    'widget' => array(
      'weight' => '5',
      'type' => 'text_textarea_with_summary',
      'module' => 'text',
      'active' => 1,
      'settings' => array(
        'rows' => '4',
        'maxlength_js_summary' => '280',
        'maxlength_js_label_summary' => 'Veuillez décrire l\'essentiel de votre conférence, en @limit caractères, il en reste :  <strong>@remaining</strong>',
        'maxlength_js' => '280',
        'maxlength_js_enforce' => 1,
        'maxlength_js_truncate_html' => 0,
        'maxlength_js_label' => 'Veuillez décrire l\'essentiel de votre conférence, en @limit caractères, il en reste :  <strong>@remaining</strong>
',
        'summary_rows' => 5,
      ),
    ),
    'settings' => array(
      'text_processing' => '0',
      'display_summary' => 0,
      'description_display' => 'before',
      'user_register_form' => FALSE,
    ),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'text_plain',
        'weight' => '3',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'text',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'text_summary_or_trimmed',
        'settings' => array(
          'trim_length' => 600,
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'text',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'above',
        'type' => 'text_trimmed',
        'weight' => '3',
        'settings' => array(
          'trim_length' => '140',
          'quickedit' => array(
            'editor' => 'form',
          ),
        ),
        'module' => 'text',
      ),
    ),
    'required' => 1,
    'description' => '',
    'default_value' => NULL,
    'field_name' => 'body',
    'entity_type' => 'node',
    'bundle' => 'sujet_de_conference',
    'deleted' => '0',
  ),
);

$dependencies = array(
  'content_type.sujet_de_conference' => 'content_type.sujet_de_conference',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'text',
);
