<?php
/**
 * @file
 * permission.manage_conf_rence_properties.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'manage conf_rence properties',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
