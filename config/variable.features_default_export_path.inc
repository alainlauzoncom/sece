<?php
/**
 * @file
 * variable.features_default_export_path.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_default_export_path',
  'content' => 'sites/all/modules/features',
);

$dependencies = array();

$optional = array();

$modules = array();
