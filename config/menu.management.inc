<?php
/**
 * @file
 * menu.management.inc
 */

$api = '2.0.0';

$data = array(
  'menu_name' => 'management',
  'title' => 'Management',
  'description' => 'Le menu administratif du site.',
);

$dependencies = array();

$optional = array();

$modules = array();
