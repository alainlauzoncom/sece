<?php
/**
 * @file
 * permission.administer_dexpmasonry.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer dexpmasonry',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'dexp_masonry',
);
