<?php
/**
 * @file
 * permission.access_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'access content',
  'roles' => array(
    0 => 'anonymous user',
    1 => 'administrator',
    2 => 'Gestionnaire',
    3 => 'Pré-autorisé',
    4 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'node',
);
