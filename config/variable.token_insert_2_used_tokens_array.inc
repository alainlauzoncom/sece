<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_array.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_array',
  'content' => array(
    '[array:keys]' => 0,
    '[array:last]' => 0,
    '[array:count]' => 0,
    '[array:join]' => 0,
    '[array:reversed]' => 0,
    '[array:join-path]' => 0,
    '[array:first]' => 0,
    '[array:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
