<?php
/**
 * @file
 * field.user.field_nombre_de_conferences_recu.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_nombre_de_conferences_recu' => array(
              'value' => 'field_nombre_de_conferences_recu_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_nombre_de_conferences_recu' => array(
              'value' => 'field_nombre_de_conferences_recu_value',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'field_name' => 'field_nombre_de_conferences_recu',
    'type' => 'number_integer',
    'module' => 'number',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Nombre de conférences reçues',
    'widget' => array(
      'weight' => '19',
      'type' => 'number',
      'module' => 'number',
      'active' => 0,
      'settings' => array(),
    ),
    'settings' => array(
      'min' => '0',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'description_display' => 'after',
      'user_register_form' => 0,
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => '11',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'form',
          ),
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'scale' => 0,
          'prefix_suffix' => TRUE,
        ),
        'module' => 'number',
      ),
    ),
    'required' => 0,
    'description' => '',
    'default_value' => array(
      0 => array(
        'value' => '0',
      ),
    ),
    'field_name' => 'field_nombre_de_conferences_recu',
    'entity_type' => 'user',
    'bundle' => 'user',
    'deleted' => '0',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'number',
);
