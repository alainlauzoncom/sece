<?php
/**
 * @file
 * views_view.champs_d_expertises.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'champs_d_expertises';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'Champs d\'expertises';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Termes de taxonomie';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'plus';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'views_calc_distinct';
$handler->display->display_options['style_options']['columns'] = array(
  'name_1' => 'name_1',
  'name' => 'name',
  'field_nombre_de_conferences_dema' => 'field_nombre_de_conferences_dema',
  'field_nombre_de_conferences_offe' => 'field_nombre_de_conferences_offe',
);
$handler->display->display_options['style_options']['default'] = 'name';
$handler->display->display_options['style_options']['info'] = array(
  'name_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 0,
    'calc' => array(),
  ),
  'field_nombre_de_conferences_dema' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 1,
    'calc' => array(
      'SUM' => 'SUM',
    ),
  ),
  'field_nombre_de_conferences_offe' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    'has_calc' => 1,
    'calc' => array(
      'SUM' => 'SUM',
    ),
  ),
);
$handler->display->display_options['style_options']['detailed_values'] = '0';
$handler->display->display_options['style_options']['precision'] = '0';
$handler->display->display_options['style_options']['views_calc_distinct'] = 1;
/* Entête: Global : PHP */
$handler->display->display_options['header']['php']['id'] = 'php';
$handler->display->display_options['header']['php']['table'] = 'views';
$handler->display->display_options['header']['php']['field'] = 'php';
/* Relation: Sujet de conférence */
$handler->display->display_options['relationships']['nid']['id'] = 'nid';
$handler->display->display_options['relationships']['nid']['table'] = 'taxonomy_index';
$handler->display->display_options['relationships']['nid']['field'] = 'nid';
$handler->display->display_options['relationships']['nid']['ui_name'] = 'Sujet de conférence';
$handler->display->display_options['relationships']['nid']['label'] = 'Sujet de conférence';
/* Champ: Vocabulaire de taxonomie : Nom */
$handler->display->display_options['fields']['name_1']['id'] = 'name_1';
$handler->display->display_options['fields']['name_1']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['fields']['name_1']['field'] = 'name';
$handler->display->display_options['fields']['name_1']['label'] = 'Vocabulaire';
/* Champ: Terme de taxonomie : Nom */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'Terme de taxonomie';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
/* Champ: Terme de taxonomie : Nombre de conférences demandées */
$handler->display->display_options['fields']['field_nombre_de_conferences_dema']['id'] = 'field_nombre_de_conferences_dema';
$handler->display->display_options['fields']['field_nombre_de_conferences_dema']['table'] = 'field_data_field_nombre_de_conferences_dema';
$handler->display->display_options['fields']['field_nombre_de_conferences_dema']['field'] = 'field_nombre_de_conferences_dema';
$handler->display->display_options['fields']['field_nombre_de_conferences_dema']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Champ: Terme de taxonomie : Nombre de conférences offertes */
$handler->display->display_options['fields']['field_nombre_de_conferences_offe']['id'] = 'field_nombre_de_conferences_offe';
$handler->display->display_options['fields']['field_nombre_de_conferences_offe']['table'] = 'field_data_field_nombre_de_conferences_offe';
$handler->display->display_options['fields']['field_nombre_de_conferences_offe']['field'] = 'field_nombre_de_conferences_offe';
$handler->display->display_options['fields']['field_nombre_de_conferences_offe']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Critère de filtrage: Vocabulaire de taxonomie : Nom système */
$handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['value'] = array(
  'categorie' => 'categorie',
  'thematique_d_intervention_privilegiee' => 'thematique_d_intervention_privilegiee',
);
$handler->display->display_options['filters']['machine_name']['group'] = 1;
$handler->display->display_options['filters']['machine_name']['exposed'] = TRUE;
$handler->display->display_options['filters']['machine_name']['expose']['operator_id'] = 'machine_name_op';
$handler->display->display_options['filters']['machine_name']['expose']['label'] = 'Taxonomie';
$handler->display->display_options['filters']['machine_name']['expose']['operator'] = 'machine_name_op';
$handler->display->display_options['filters']['machine_name']['expose']['identifier'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['machine_name']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['machine_name']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['machine_name']['expose']['remember_roles'] = array(
  2 => 0,
  3 => '3',
  4 => '4',
  1 => 0,
  5 => 0,
  6 => 0,
);
$handler->display->display_options['filters']['machine_name']['expose']['reduce'] = TRUE;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/reports/termes-de-taxonomie';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Termes de taxonomie';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['champs_d_expertises'] = array(
  t('Master'),
  t('Termes de taxonomie'),
  t('plus'),
  t('Appliquer'),
  t('Réinitialiser'),
  t('Trier par'),
  t('Asc'),
  t('Desc'),
  t('Sujet de conférence'),
  t('Vocabulaire'),
  t('Terme de taxonomie'),
  t('Nombre de conférences demandées'),
  t('Nombre de conférences offertes'),
  t('Taxonomie'),
  t('Page'),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'taxonomy',
  2 => 'number',
);
