<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_list_rules_config_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_list<rules_config>',
  'content' => array(
    '[list<rules_config>:0]' => 0,
    '[list<rules_config>:1]' => 0,
    '[list<rules_config>:2]' => 0,
    '[list<rules_config>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
