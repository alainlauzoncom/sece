<?php
/**
 * @file
 * permission.administer_fields.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer fields',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field',
);
