<?php
/**
 * @file
 * permission.use_workbench_moderation_my_drafts_tab.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'use workbench_moderation my drafts tab',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
