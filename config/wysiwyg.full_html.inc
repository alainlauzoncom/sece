<?php
/**
 * @file
 * wysiwyg.full_html.inc
 */

$api = '2.0.0';

$data = (object) array(
  'format' => 'full_html',
  'editor' => 'tinymce',
  'settings' => array(
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 1,
    'theme' => 'advanced',
    'language' => 'en',
    'buttons' => array(
      'default' => array(
        'bold' => 1,
        'italic' => 1,
        'underline' => 1,
        'justifyleft' => 1,
        'justifycenter' => 1,
        'justifyright' => 1,
        'justifyfull' => 1,
        'bullist' => 1,
        'numlist' => 1,
        'outdent' => 1,
        'indent' => 1,
        'anchor' => 1,
        'cleanup' => 1,
        'formatselect' => 1,
        'backcolor' => 1,
        'sup' => 1,
        'sub' => 1,
        'blockquote' => 1,
        'code' => 1,
        'cut' => 1,
        'copy' => 1,
        'paste' => 1,
        'removeformat' => 1,
      ),
      'advhr' => array(
        'advhr' => 1,
      ),
      'advimage' => array(
        'advimage' => 1,
      ),
      'advlink' => array(
        'advlink' => 1,
      ),
      'contextmenu' => array(
        'contextmenu' => 1,
      ),
      'fullscreen' => array(
        'fullscreen' => 1,
      ),
      'paste' => array(
        'pastetext' => 1,
        'selectall' => 1,
      ),
      'preview' => array(
        'preview' => 1,
      ),
      'searchreplace' => array(
        'search' => 1,
        'replace' => 1,
      ),
      'table' => array(
        'tablecontrols' => 1,
      ),
      'xhtmlxtras' => array(
        'cite' => 1,
        'del' => 1,
        'abbr' => 1,
        'acronym' => 1,
        'ins' => 1,
        'attribs' => 1,
      ),
      'advlist' => array(
        'advlist' => 1,
      ),
      'wordcount' => array(
        'wordcount' => 1,
      ),
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'left',
    'path_loc' => 'bottom',
    'resizing' => 1,
    'verify_html' => 1,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 1,
    'remove_linebreaks' => 0,
    'apply_source_formatting' => 0,
    'paste_auto_cleanup_on_paste' => 1,
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'css_setting' => 'theme',
    'css_path' => '',
    'css_classes' => '',
  ),
  'rdf_mapping' => array(),
);

$dependencies = array(
  'text_format.full_html' => 'text_format.full_html',
);

$optional = array();

$modules = array(
  0 => 'wysiwyg',
);
