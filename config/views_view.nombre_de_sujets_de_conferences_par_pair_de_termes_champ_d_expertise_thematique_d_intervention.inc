<?php
/**
 * @file
 * views_view.nombre_de_sujets_de_conferences_par_pair_de_termes_champ_d_expertise_thematique_d_intervention.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'nombre_de_sujets_de_conferences_par_pair_de_termes_champ_d_expertise_thematique_d_intervention';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'eck_sece_nombre_de_sujets_par_pair';
$view->human_name = 'Nombre de sujets de conférences par pair de termes Champ d\'expertise / Thématique d\'intervention';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Nombre de sujets de conférences par pair de termes Champ d\'expertise / Thématique d\'intervention';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'plus';
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  4 => '4',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
$handler->display->display_options['pager']['type'] = 'infinite_scroll';
$handler->display->display_options['pager']['options']['items_per_page'] = '0';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_pair' => 'field_pair',
  'field_nombre_de_sujets_offerts' => 'field_nombre_de_sujets_offerts',
  'field_nombre_de_sujets_demandes' => 'field_nombre_de_sujets_demandes',
);
$handler->display->display_options['style_options']['default'] = 'field_nombre_de_sujets_offerts';
$handler->display->display_options['style_options']['info'] = array(
  'field_pair' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_nombre_de_sujets_offerts' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_nombre_de_sujets_demandes' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Champ: Nombre de sujets de conférence par pair de termes champ/thématique : pair */
$handler->display->display_options['fields']['field_pair']['id'] = 'field_pair';
$handler->display->display_options['fields']['field_pair']['table'] = 'field_data_field_pair';
$handler->display->display_options['fields']['field_pair']['field'] = 'field_pair';
$handler->display->display_options['fields']['field_pair']['label'] = 'Combinaison';
/* Champ: Nombre de sujets de conférence par pair de termes champ/thématique : Nombre de sujets offerts */
$handler->display->display_options['fields']['field_nombre_de_sujets_offerts']['id'] = 'field_nombre_de_sujets_offerts';
$handler->display->display_options['fields']['field_nombre_de_sujets_offerts']['table'] = 'field_data_field_nombre_de_sujets_offerts';
$handler->display->display_options['fields']['field_nombre_de_sujets_offerts']['field'] = 'field_nombre_de_sujets_offerts';
$handler->display->display_options['fields']['field_nombre_de_sujets_offerts']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Champ: Nombre de sujets de conférence par pair de termes champ/thématique : Nombre de sujets demandés */
$handler->display->display_options['fields']['field_nombre_de_sujets_demandes']['id'] = 'field_nombre_de_sujets_demandes';
$handler->display->display_options['fields']['field_nombre_de_sujets_demandes']['table'] = 'field_data_field_nombre_de_sujets_demandes';
$handler->display->display_options['fields']['field_nombre_de_sujets_demandes']['field'] = 'field_nombre_de_sujets_demandes';
$handler->display->display_options['fields']['field_nombre_de_sujets_demandes']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Critère de filtrage: Nombre de sujets de conférence par pair de termes champ/thématique : sece_nombre_de_sujets_par_pair type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'eck_sece_nombre_de_sujets_par_pair';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'sece_nombre_de_sujets_par_pair' => 'sece_nombre_de_sujets_par_pair',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/report/nombre-de-sujets-de-conf-rences-par-pair-de-termes-champ-d-expertise-th-matique-d-intervention';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Combinaisons Champ d\'expertise / Thématique d\'intervention';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['nombre_de_sujets_de_conferences_par_pair_de_termes_champ_d_expertise_thematique_d_intervention'] = array(
  t('Master'),
  t('Nombre de sujets de conférences par pair de termes Champ d\'expertise / Thématique d\'intervention'),
  t('plus'),
  t('Appliquer'),
  t('Réinitialiser'),
  t('Trier par'),
  t('Asc'),
  t('Desc'),
  t('Show More'),
  t('Combinaison'),
  t('Nombre de sujets offerts'),
  t('Nombre de sujets demandés'),
  t('Page'),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'eck',
  2 => 'text',
  3 => 'number',
);
