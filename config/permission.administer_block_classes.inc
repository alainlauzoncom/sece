<?php
/**
 * @file
 * permission.administer_block_classes.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer block classes',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'block_class',
);
