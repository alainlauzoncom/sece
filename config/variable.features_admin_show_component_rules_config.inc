<?php
/**
 * @file
 * variable.features_admin_show_component_rules_config.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_show_component_rules_config',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
