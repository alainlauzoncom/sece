<?php
/**
 * @file
 * permission.administer_statistics.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer statistics',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'statistics',
);
