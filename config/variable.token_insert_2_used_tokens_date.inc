<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_date.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_date',
  'content' => array(
    '[date:short]' => 0,
    '[date:custom]' => 0,
    '[date:long]' => 0,
    '[date:medium]' => 0,
    '[date:raw]' => 0,
    '[date:since]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
