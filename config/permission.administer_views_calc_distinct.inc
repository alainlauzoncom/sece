<?php
/**
 * @file
 * permission.administer_views_calc_distinct.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer views calc distinct',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views_calc_distinct',
);
