<?php
/**
 * @file
 * permission.delete_own_sujet_de_conference_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete own sujet_de_conference content',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array(
  'content_type.sujet_de_conference' => 'content_type.sujet_de_conference',
);

$optional = array();

$modules = array(
  0 => 'node',
);
