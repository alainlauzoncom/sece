<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_menu.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_menu',
  'content' => array(
    '[menu:description]' => 0,
    '[menu:edit-url]' => 0,
    '[menu:name]' => 0,
    '[menu:menu-link-count]' => 0,
    '[menu:machine-name]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
