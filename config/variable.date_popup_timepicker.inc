<?php
/**
 * @file
 * variable.date_popup_timepicker.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'date_popup_timepicker',
  'content' => 'timepicker',
);

$dependencies = array();

$optional = array();

$modules = array();
