<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_list_conf_rence_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_list<conf_rence>',
  'content' => array(
    '[list<conf_rence>:0]' => 0,
    '[list<conf_rence>:1]' => 0,
    '[list<conf_rence>:2]' => 0,
    '[list<conf_rence>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
