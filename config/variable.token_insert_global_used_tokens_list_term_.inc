<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_list_term_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_list<term>',
  'content' => array(
    '[list<term>:0]' => 0,
    '[list<term>:1]' => 0,
    '[list<term>:2]' => 0,
    '[list<term>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
