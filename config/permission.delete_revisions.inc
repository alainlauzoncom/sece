<?php
/**
 * @file
 * permission.delete_revisions.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete revisions',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'node',
);
