<?php
/**
 * @file
 * variable.features_admin_show_component_image.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_show_component_image',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
