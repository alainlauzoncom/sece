<?php
/**
 * @file
 * permission.submit_translations_to_localization_server.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'submit translations to localization server',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'l10n_client',
);
