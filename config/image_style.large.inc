<?php
/**
 * @file
 * image_style.large.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'large',
  'label' => 'Large (480x480)',
  'effects' => array(
    0 => array(
      'label' => 'Échelle',
      'help' => 'La mise à l\'échelle maintiendra les proportions originales de l\'image. Si une seule dimension est précisée, l\'autre dimension sera calculée automatiquement.',
      'effect callback' => 'image_scale_effect',
      'dimensions callback' => 'image_scale_dimensions',
      'form callback' => 'image_scale_form',
      'summary theme' => 'image_scale_summary',
      'module' => 'image',
      'name' => 'image_scale',
      'data' => array(
        'width' => 480,
        'height' => 480,
        'upscale' => 0,
      ),
      'weight' => '0',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'image',
);
