<?php
/**
 * @file
 * variable.user_mail_register_admin_created_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_register_admin_created_body',
  'content' => 'Bonjour [user:name-raw],

Merci de vous être inscrit au système d’échange de conférences en EDDEC (SECE). Votre inscription a été validée par l’Institut EDDEC.

Veuillez cliquer sur le lien ci-dessous pour activer votre compte :

[user:one-time-login-url]

Si vous n’avez pas inscrit de demande ou d’offre de conférence, l’Institut EDDEC vous invite à venir bonifier votre fiche d’inscription au cours des 10 prochains jours afin d’assurer le dynamisme et le bon fonctionnement du SECE. 

Pour toute question relative au SECE et à son utilisation, n’hésitez pas à communiquer avec l’Institut EDDEC.

Bons échanges!

Stéphanie Jagou
Chargée de projets en développement durable et communications
stephanie.jagou@instituteddec.org
Tél. : +1 514 340.4711, poste 2150
Institut EDDEC – environnement, développement durable, économie circulaire
Université de Montréal, HEC Montréal, Polytechnique Montréal
www.instituteddec.org',
);

$dependencies = array();

$optional = array();

$modules = array();
