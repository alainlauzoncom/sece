<?php
/**
 * @file
 * variable.user_mail_register_no_approval_required_subject.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_register_no_approval_required_subject',
  'content' => 'Détails du compte [user:field_prenom] [user:field_nom] sur le Système d\'échange de conférences en EDDEC (SECE)',
);

$dependencies = array();

$optional = array();

$modules = array();
