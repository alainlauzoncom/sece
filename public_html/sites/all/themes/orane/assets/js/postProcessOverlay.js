jQuery(document).ready(function($) {
    var link = $("#new-password-link").attr('href');
    newlink = addDestinationToLink(link);
    $("#new-password-link").attr('href', newlink);

    var link = $("#register-link").attr('href');
    newlink = addDestinationToLink(link);
    $("#register-link").attr('href', newlink);
});

function addDestinationToLink(link) {
    var pathname = window.parent.location.pathname;
    pathname = pathname.substring(1);
    return link + '?destination='+pathname;
}