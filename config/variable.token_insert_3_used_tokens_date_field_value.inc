<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_date_field_value.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_date-field-value',
  'content' => array(
    '[date-field-value:date]' => 0,
    '[date-field-value:to-date]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
