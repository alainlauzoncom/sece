<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_list_node_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_list<node>',
  'content' => array(
    '[list<node>:0]' => 0,
    '[list<node>:1]' => 0,
    '[list<node>:2]' => 0,
    '[list<node>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
