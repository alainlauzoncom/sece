<?php
/**
 * @file
 * permission.translate_interface.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'translate interface',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'locale',
);
