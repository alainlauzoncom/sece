<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_list_workbench_moderation_transition_.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_list<workbench_moderation_transition>',
  'content' => array(
    '[list<workbench_moderation_transition>:0]' => 0,
    '[list<workbench_moderation_transition>:1]' => 0,
    '[list<workbench_moderation_transition>:2]' => 0,
    '[list<workbench_moderation_transition>:3]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
