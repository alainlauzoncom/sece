<?php
/**
 * @file
 * permission.eck_list_sece_nombre_de_sujets_par_pair_sece_nombre_de_sujets_par_pair_entities.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck list sece_nombre_de_sujets_par_pair sece_nombre_de_sujets_par_pair entities',
  'roles' => array(
    0 => 'administrator',
    1 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
