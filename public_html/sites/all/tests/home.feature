Feature: Home
  In order to see the website
  As an anonymous user
  I need to see the home page

  @javascript
  Scenario: Frontpage
    Given I am not logged in
    And I am on the homepage
    Then I should see "Nom d'utilisateur"
    And I should see "Mot de passe"
    And I should see "Accueil"
    And I should see "Principes"
    And I should see "Sujets de conférence offerts"
    And I should see "Sujets de conférence demandés"
    And I should see "Inscription"
    And I should see "Contact"

  @javascript
  Scenario: Page Principes
    Given I am not logged in
    And I am on the homepage
    When I follow "Principes"
    Then I should be on "/g%C3%A9n%C3%A9ralit%C3%A9s"
    And I should see "Accueil"
    And I should see "Principes"
    And I should see "Sujets de conférence offerts"
    And I should see "Sujets de conférence demandés"
    And I should see "Inscription"
    And I should see "Contact"
    And I should see "Généralités"
    And I should see "Enseignants et cours visés"
    And I should see "Modalités de la conférence"
    And I should see "Modalités de fonctionnement du système"

  @javascript
  Scenario: Page Sujets de conférence offerts
    Given I am not logged in
    And I am logged in with the role "administrator"
    And I am on the homepage
    When I follow "Sujets de conférence offerts"
    Then I should be on "/sujets-de-conference-offerts"
    And I should see "Accueil"
    And I should see "Principes"
    And I should see "Sujets de conférence offerts"
    And I should see "Sujets de conférence demandés"
    And I should see "Inscription"
    And I should see "Contact"
    And I should see "Nom, prénom et/ou adresse courriel"
    And I should see "Titre"
    And I should see "Catégories"
    And I should see "Thématiques d'intervention privilégiées"