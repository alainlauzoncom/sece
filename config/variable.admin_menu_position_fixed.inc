<?php
/**
 * @file
 * variable.admin_menu_position_fixed.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'admin_menu_position_fixed',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
