<?php
/**
 * @file
 * permission.access_workbench.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'access workbench',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench',
);
