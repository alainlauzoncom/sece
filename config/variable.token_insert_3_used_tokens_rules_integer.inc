<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_rules_integer.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_rules_integer',
  'content' => array(
    '[rules_integer:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
