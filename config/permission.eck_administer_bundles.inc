<?php
/**
 * @file
 * permission.eck_administer_bundles.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck administer bundles',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
