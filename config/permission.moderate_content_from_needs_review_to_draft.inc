<?php
/**
 * @file
 * permission.moderate_content_from_needs_review_to_draft.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'moderate content from needs_review to draft',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
