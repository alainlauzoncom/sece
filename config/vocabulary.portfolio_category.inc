<?php
/**
 * @file
 * vocabulary.portfolio_category.inc
 */

$api = '2.0.0';

$data = (object) array(
  'vid' => '6',
  'name' => 'Portfolio Category',
  'machine_name' => 'portfolio_category',
  'description' => 'List of categories used to filter the portfolio',
  'hierarchy' => '0',
  'module' => 'taxonomy',
  'weight' => '0',
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'skos:ConceptScheme',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'rdfs:comment',
      ),
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
