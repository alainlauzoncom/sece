<?php
/**
 * @file
 * variable.language_default.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'language_default',
  'content' => (object) array(
    'language' => 'fr',
    'name' => 'French',
    'native' => 'Français',
    'direction' => '0',
    'enabled' => '1',
    'plurals' => '2',
    'formula' => '($n>1)',
    'domain' => '',
    'prefix' => 'fr',
    'weight' => '-10',
    'javascript' => 'HBooWi88uAiVBDuamSCo1Ds5Po8CvmoXa9iOfEJYvrQ',
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
