<?php
/**
 * @file
 * variable.field_bundle_settings_sece_nombre_de_sujets_par_pair__sece_nombre_de_sujets_par_pair.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'field_bundle_settings_sece_nombre_de_sujets_par_pair__sece_nombre_de_sujets_par_pair',
  'content' => array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
