<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_current_date.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_current-date',
  'content' => array(
    '[current-date:short]' => 0,
    '[current-date:custom]' => 0,
    '[current-date:long]' => 0,
    '[current-date:medium]' => 0,
    '[current-date:raw]' => 0,
    '[current-date:since]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
