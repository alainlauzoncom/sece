<?php
/**
 * @file
 * variable.field_bundle_settings_file__audio.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'field_bundle_settings_file__audio',
  'content' => array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'file' => array(
          'media_small' => array(
            'weight' => 0,
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
