<?php
/**
 * @file
 * permission.bypass_workbench_moderation.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'bypass workbench moderation',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
