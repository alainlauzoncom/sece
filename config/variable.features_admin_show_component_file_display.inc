<?php
/**
 * @file
 * variable.features_admin_show_component_file_display.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'features_admin_show_component_file_display',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
