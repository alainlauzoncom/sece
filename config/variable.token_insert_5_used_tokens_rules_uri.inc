<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_rules_uri.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_rules_uri',
  'content' => array(
    '[rules_uri:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
