<?php
/**
 * @file
 * permission.moderate_content_from_retired_to_draft.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'moderate content from retired to draft',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
    2 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
