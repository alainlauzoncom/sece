<?php

drupal_set_message('sece_overlay_windows_handler_field_links.inc included!');

/**
 * @file
 * Provides moderation links for Views.
 */

class sece_overlay_windows_handler_field_links extends views_handler_field {
  function render($values) {
    if ($values->{$this->aliases['is_current']}) {
      $node = node_load($values->{$this->aliases['nid']}, $values->{$this->aliases['vid']});
      return theme('links', array('links' => sece_overlay_windows_get_moderation_links($node, array('html' => TRUE, 'query' => array('destination' => $_GET['q'])))));
    }
    return '';
  }
}
