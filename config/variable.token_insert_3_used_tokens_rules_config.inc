<?php
/**
 * @file
 * variable.token_insert_3_used_tokens_rules_config.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_3_used_tokens_rules_config',
  'content' => array(
    '[rules_config:access-exposed]' => 0,
    '[rules_config:active]' => 0,
    '[rules_config:dirty]' => 0,
    '[rules_config:id]' => 0,
    '[rules_config:module]' => 0,
    '[rules_config:owner]' => 0,
    '[rules_config:plugin]' => 0,
    '[rules_config:status]' => 0,
    '[rules_config:weight]' => 0,
    '[rules_config:label]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
