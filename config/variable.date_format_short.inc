<?php
/**
 * @file
 * variable.date_format_short.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'date_format_short',
  'content' => 'd/m/Y',
);

$dependencies = array();

$optional = array();

$modules = array();
