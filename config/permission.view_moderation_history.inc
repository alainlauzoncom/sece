<?php
/**
 * @file
 * permission.view_moderation_history.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'view moderation history',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
    2 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'workbench_moderation',
);
