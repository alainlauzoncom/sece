<?php
/**
 * @file
 * variable.user_mail_register_pending_approval_subject.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_register_pending_approval_subject',
  'content' => 'Détails du compte [user:name-raw] sur [site:name] (en attente d\'approbation)',
);

$dependencies = array();

$optional = array();

$modules = array();
