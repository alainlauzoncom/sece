Textarea maxlength js widget module
===================================

DESCRIPTION
------------

This module provides a custom widget for the text and text_long field types.


Features include:
 * Recommended text length, which will allow the user to specify a recommended
text length.

REQUIREMENTS
------------
Drupal 7.x

INSTALLATION
------------
1.  Place Textarea maxlength JS module into your modules directory.
    This is normally the "sites/all/modules" directory.

2.  Go to admin/build/modules. Enable the module.

Read more about installing modules at http://drupal.org/node/70151

UPGRADING
---------
Any updates should be automatic.
