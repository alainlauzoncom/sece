<?php
/**
 * @file
 * variable.field_bundle_settings_conf_rence__conf_rence.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'field_bundle_settings_conf_rence__conf_rence',
  'content' => array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(),
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
