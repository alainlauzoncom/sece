<?php
/**
 * @file
 * content_type.sujet_de_conference.inc
 */

$api = '2.0.0';

$data = (object) array(
  'type' => 'sujet_de_conference',
  'name' => 'Sujet de conférence',
  'description' => 'C\'est la description d\'une conférence offerte ou demandée',
  'has_title' => '1',
  'title_label' => 'Titre',
  'base' => 'node_content',
  'help' => '',
);

$dependencies = array(
  'variable.field_bundle_settings_node__sujet_de_conference' => 'variable.field_bundle_settings_node__sujet_de_conference',
  'variable.language_content_type_sujet_de_conference' => 'variable.language_content_type_sujet_de_conference',
  'variable.node_options_sujet_de_conference' => 'variable.node_options_sujet_de_conference',
  'variable.node_preview_sujet_de_conference' => 'variable.node_preview_sujet_de_conference',
  'variable.node_submitted_sujet_de_conference' => 'variable.node_submitted_sujet_de_conference',
);

$optional = array(
  'field.node.body.sujet_de_conference' => 'field.node.body.sujet_de_conference',
  'field.node.field_type.sujet_de_conference' => 'field.node.field_type.sujet_de_conference',
  'field.node.field_categories.sujet_de_conference' => 'field.node.field_categories.sujet_de_conference',
  'field.node.field_thematiques_d_intervention.sujet_de_conference' => 'field.node.field_thematiques_d_intervention.sujet_de_conference',
  'field.node.field_donnee.sujet_de_conference' => 'field.node.field_donnee.sujet_de_conference',
  'field.node.title_field.sujet_de_conference' => 'field.node.title_field.sujet_de_conference',
  'field.node.field_ncd_sujet_de_conference.sujet_de_conference' => 'field.node.field_ncd_sujet_de_conference.sujet_de_conference',
  'field.node.field_autre_champ_d_expertise.sujet_de_conference' => 'field.node.field_autre_champ_d_expertise.sujet_de_conference',
  'field.node.field_autre_thematique.sujet_de_conference' => 'field.node.field_autre_thematique.sujet_de_conference',
  'permission.create_sujet_de_conference_content' => 'permission.create_sujet_de_conference_content',
  'permission.edit_own_sujet_de_conference_content' => 'permission.edit_own_sujet_de_conference_content',
  'permission.edit_any_sujet_de_conference_content' => 'permission.edit_any_sujet_de_conference_content',
  'permission.delete_own_sujet_de_conference_content' => 'permission.delete_own_sujet_de_conference_content',
  'permission.delete_any_sujet_de_conference_content' => 'permission.delete_any_sujet_de_conference_content',
);

$modules = array(
  0 => 'node',
);
