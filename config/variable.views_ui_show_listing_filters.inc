<?php
/**
 * @file
 * variable.views_ui_show_listing_filters.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'views_ui_show_listing_filters',
  'content' => 1,
);

$dependencies = array();

$optional = array();

$modules = array();
