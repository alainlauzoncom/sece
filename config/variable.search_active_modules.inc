<?php
/**
 * @file
 * variable.search_active_modules.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'search_active_modules',
  'content' => array(
    'node' => 'node',
    'advanced_help' => 0,
    'user' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
