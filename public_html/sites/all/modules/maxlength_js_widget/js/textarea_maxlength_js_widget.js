(function($) {

  // Make everything local
  var ml = ml || {};
  ml.options = ml.options || {};

  Drupal.behaviors.textarea_maxlength_js_widget = {
    attach: function(context, settings) {

      if (Drupal.wysiwyg != undefined) {
        $.each(Drupal.wysiwyg.editor.init, function(editor) {
          if (typeof ml[editor] == 'function') {
            ml[editor]();
          }
        });
      }
      $('.textarea-maxlength-js', context).once('textarea-maxlength-js', function() {
        // Pass a sibling to the charCount() to test if the grippie to resize textarea exists.
        // If so, just insert the char count after the grippie.
        var $sibling = $(this).next();
        if ($($sibling).hasClass('grippie')) {
          $(this).charCount($sibling);
        }
      });
    },
    detach: function(context, settings) {
      $('.textarea-maxlength-js', context).removeOnce('textarea-maxlength-js', function() {
        $(this).charCount({
          action: 'detach'
        });
      });
    }
  };

  /**
   * Code below is based on:
   *   Character Count Plugin - jQuery plugin
   *   Dynamic character count for text areas and input fields
   *   written by Alen Grakalic
   *   http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas
   */

  ml.calculate = function(obj, options, count) {
    var counter = $('#' + obj.attr('id') + '-' + options.css);
    var limit = parseInt(obj.attr('maxlength_js'));

    if (count == undefined) {
      count = ml.strlen(obj.val());
    }

    var available = count;

    if (available >= options.warning) {
      counter.addClass(options.cssWarning);
    }
    else {
      counter.removeClass(options.cssWarning);
    }

    if (available > limit) {
      counter.addClass(options.cssExceeded);
      // Trim text
      if (options.enforce === true) {
        obj.val(obj.val().substr(0, limit));
        // Re calculate text length
        count = ml.strlen(obj.val());
      }
    }
    else {
      counter.removeClass(options.cssExceeded);
    }

    if (limit) {
      counter.html(options.counterText + available + ' / ' + limit);
    }
    else {
      counter.html(options.counterText + available);
    }

  };

  /**
   * Calculate the length of a string using PHP-calculation to count new lines
   * as two characters.
   *
   * @see http://www.sitepoint.com/blogs/2004/02/16/line-endings-in-javascript/
   */
  ml.strlen = function(str) {
    return str.replace(/(\r\n|\r|\n)/g, "\r\n").length;
  };

  $.fn.charCount = function(selector, options) {
    if (typeof selector == "undefined") {
      selector = $(this);
    }

    // default configuration properties
    var defaults = {
      warning: 25,
      css: 'counter',
      counterElement: 'span',
      cssWarning: 'warning',
      cssExceeded: 'exceeded',
      counterText: Drupal.t('Number of characters: '),
      action: 'attach',
      enforce: false
    };

    var options = $.extend(defaults, options);
    ml.options[$(this).attr('id')] = options;

    if (options.action == 'detach') {
      $(this).removeClass('maxlength-js-processed');
      $('#' + $(this).attr('id') + '-' + options.css).remove();
      delete ml.options[$(this).attr('id')];
      return 'removed';
    }

    var id = $(this).attr('id') + '-' + options.css;
    if (!$(options.counterElement + '#' + id).length) {
      $(selector).after('<' + options.counterElement + ' id="' + id + '" class="' + options.css + '"></' + options.counterElement + '>');
      ml.calculate($(this), options);
      $(this).keyup(function() {
        ml.calculate($(this), options);
      });
      $(this).change(function() {
        ml.calculate($(this), options);
      });
    }

  };

  /**
   * Integrate with WYSIWYG
   * Detect changes on editors and invoke ml.counter()
   */
  ml.tinymce = function() {
    // We only run it once
    var onlyOnce = false;
    if (!onlyOnce) {
      onlyOnce = true;
      tinyMCE.onAddEditor.add(function(mgr,ed) {
          // the editor is on a maxlength field
          if ($('#' + ed.editorId + '.maxlength-js').length == 1) {
            ed.onChange.add(function(ed, l) {
              ml.tinymceChange(ed);
            });
            ed.onKeyUp.add(function(ed, e) {
              ml.tinymceChange(ed);
            });
            ed.onPaste.add(function(ed, e) {
              setTimeout(function(){ml.tinymceChange(ed)}, 500);
            });
          }
      });
    }
  }
  // Invoke ml.counter() for editor
  ml.tinymceChange = function(ed) {
    // CLone to avoid changing defaults
    var options = $.extend({}, ml.options[ed.editorId]);
    // We can't enforce with WYSIWYG
    // HTML makes it hard to control withouth breaking it
    options.enforce = false;
    ml.calculate($(ed.getElement()), options, ml.strlen(ed.getContent()));
  };

  /**
   * Integrate with ckEditor
   * Detect changes on editors and invoke ml.counter()
   */
  ml.ckeditor = function() {
    // We only run it once
    var onlyOnce = false;
    if (!onlyOnce) {
      onlyOnce = true;
      CKEDITOR.on('instanceReady', function(e) {
        if ($('#' + e.editor.name + '.maxlength-js').length == 1) {
          ml.options[e.editor.element.getId()].enforce == false;
          e.editor.on('key', function(e) {
            setTimeout(function(){ml.ckeditorChange(e)}, 500);
          });
          e.editor.on('paste', function(e) {
            setTimeout(function(){ml.ckeditorChange(e)}, 500);
          });
        }
      });

    }
  }
  // Invoke ml.counter() for editor
  ml.ckeditorChange = function(e) {
    // Clone to avoid changing defaults
    var options = $.extend({}, ml.options[e.editor.element.getId()]);
    // We can't enforce with WYSIWYG
    // HTML makes it hard to control withouth breaking it
    options.enforce = false;
    ml.calculate($('#' + e.editor.element.getId()), options, ml.strlen(e.editor.getData()));
  };

})(jQuery);
