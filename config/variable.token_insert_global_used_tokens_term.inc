<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_term.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_term',
  'content' => array(
    '[term:parents-all]' => 0,
    '[term:node-count]' => 0,
    '[term:description]' => 0,
    '[term:tid]' => 0,
    '[term:edit-url]' => 0,
    '[term:name]' => 0,
    '[term:parents]' => 0,
    '[term:weight]' => 0,
    '[term:parent]' => 0,
    '[term:root]' => 0,
    '[term:url]' => 0,
    '[term:vocabulary]' => 0,
    '[term:original]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
