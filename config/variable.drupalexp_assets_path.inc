<?php
/**
 * @file
 * variable.drupalexp_assets_path.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'drupalexp_assets_path',
  'content' => '/sites/all/themes/orane/assets',
);

$dependencies = array();

$optional = array();

$modules = array();
