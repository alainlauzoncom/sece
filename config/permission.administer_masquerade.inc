<?php
/**
 * @file
 * permission.administer_masquerade.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer masquerade',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'masquerade',
);
