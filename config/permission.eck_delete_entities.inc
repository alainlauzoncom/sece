<?php
/**
 * @file
 * permission.eck_delete_entities.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck delete entities',
  'roles' => array(
    0 => 'administrator',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
