<?php
/**
 * @file
 * variable.user_mail_status_canceled_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_status_canceled_body',
  'content' => '[user:name],

Votre compte sur [site:name] a été annulé.

--  L\'équipe [site:name]',
);

$dependencies = array();

$optional = array();

$modules = array();
