<?php
/**
 * @file
 * permission.search_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'search content',
  'roles' => array(
    0 => 'anonymous user',
    1 => 'authenticated user',
    2 => 'administrator',
    3 => 'Gestionnaire',
    4 => 'Pré-autorisé',
    5 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'search',
);
