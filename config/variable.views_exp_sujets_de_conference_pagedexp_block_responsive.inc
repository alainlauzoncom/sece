<?php
/**
 * @file
 * variable.views_exp_sujets_de_conference_pagedexp_block_responsive.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'views-exp-sujets_de_conference-pagedexp_block_responsive',
  'content' => array(
    'hphone' => 0,
    'vphone' => 0,
    'htablet' => 0,
    'vtablet' => 0,
    'hdesktop' => 0,
    'vdesktop' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
