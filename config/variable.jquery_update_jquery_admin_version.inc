<?php
/**
 * @file
 * variable.jquery_update_jquery_admin_version.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'jquery_update_jquery_admin_version',
  'content' => '1.7',
);

$dependencies = array();

$optional = array();

$modules = array();
