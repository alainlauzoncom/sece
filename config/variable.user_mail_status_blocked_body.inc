<?php
/**
 * @file
 * variable.user_mail_status_blocked_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_status_blocked_body',
  'content' => '[user:name],

Votre compte sur [site:name] a été bloqué.

--  L\'équipe de [site:name]',
);

$dependencies = array();

$optional = array();

$modules = array();
