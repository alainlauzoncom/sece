<?php
/**
 * @file
 * permission.administer_menu_attributes.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'administer menu attributes',
  'roles' => array(
    0 => 'administrator',
    1 => 'Gestionnaire',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'menu_attributes',
);
