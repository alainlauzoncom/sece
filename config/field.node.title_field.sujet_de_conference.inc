<?php
/**
 * @file
 * field.node.title_field.sujet_de_conference.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '1',
    'entity_types' => array(),
    'settings' => array(
      'max_length' => '100',
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_title_field' => array(
              'value' => 'title_field_value',
              'format' => 'title_field_format',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_title_field' => array(
              'value' => 'title_field_value',
              'format' => 'title_field_format',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(
      'format' => array(
        'table' => 'filter_format',
        'columns' => array(
          'format' => 'format',
        ),
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'field_name' => 'title_field',
    'type' => 'text',
    'module' => 'text',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'type' => 'varchar',
        'length' => '100',
        'not null' => FALSE,
      ),
      'format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Titre de la conférence',
    'description' => 'Il est recommandé d\'opter pour un titre court et évocateur. La description détaillée de la conférence pourra figurer dans l\'espace Description plus bas pour compléter l\'information.',
    'required' => 1,
    'settings' => array(
      'text_processing' => '0',
      'hide_label' => array(
        'page' => 0,
        'entity' => 0,
      ),
      'description_display' => 'after',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'weight' => '0',
      'type' => 'text_textfield',
      'module' => 'text',
      'active' => 1,
      'settings' => array(
        'size' => '100',
        'maxlength_js' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
    ),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'hidden',
        'weight' => '6',
        'settings' => array(),
      ),
      'full' => array(
        'type' => 'hidden',
        'label' => 'above',
        'settings' => array(),
        'weight' => 0,
      ),
      'teaser' => array(
        'type' => 'hidden',
        'label' => 'above',
        'settings' => array(),
        'weight' => 0,
      ),
    ),
    'default_value' => NULL,
    'field_name' => 'title_field',
    'entity_type' => 'node',
    'bundle' => 'sujet_de_conference',
    'deleted' => '0',
  ),
);

$dependencies = array(
  'content_type.sujet_de_conference' => 'content_type.sujet_de_conference',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'text',
);
