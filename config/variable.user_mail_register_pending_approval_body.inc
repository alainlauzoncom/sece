<?php
/**
 * @file
 * variable.user_mail_register_pending_approval_body.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_register_pending_approval_body',
  'content' => '[user:name-raw],

Merci pour votre inscription sur [site:name]. Votre demande de compte est actuellement en attente d\'approbation. Quand elle aura été acceptée, vous recevrez un autre courriel contenant les informations pour vous connecter, définir votre mot de passe et d\'autres détails.


--  L\'équipe [site:name]',
);

$dependencies = array();

$optional = array();

$modules = array();
