<?php
/**
 * @file
 * wysiwyg.ds_code.inc
 */

$api = '2.0.0';

$data = FALSE;

$dependencies = array(
  'text_format.ds_code' => 'text_format.ds_code',
);

$optional = array();

$modules = array(
  0 => 'wysiwyg',
);
