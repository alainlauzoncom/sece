# French translation of Compact Forms (7.x-1.0)
# Copyright (c) 2011 by the French translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Compact Forms (7.x-1.0)\n"
"POT-Creation-Date: 2011-01-09 06:36+0000\n"
"PO-Revision-Date: 2010-04-03 09:49+0000\n"
"Language-Team: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "User interface"
msgstr "Interface utilisateur"
msgid "characters"
msgstr "caractères"
