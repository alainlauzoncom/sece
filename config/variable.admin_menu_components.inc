<?php
/**
 * @file
 * variable.admin_menu_components.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'admin_menu_components',
  'content' => array(
    'admin_menu.icon' => TRUE,
    'admin_menu.menu' => TRUE,
    'admin_menu.search' => TRUE,
    'admin_menu.users' => TRUE,
    'admin_menu.account' => TRUE,
    'shortcut.links' => FALSE,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
