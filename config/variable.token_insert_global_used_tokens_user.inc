<?php
/**
 * @file
 * variable.token_insert_global_used_tokens_user.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_global_used_tokens_user',
  'content' => array(
    '[user:uid]' => '[user:uid]',
    '[user:field_br_ve_presentation_de_l_ex]' => 0,
    '[user:field-br-ve-presentation-de-l-ex]' => 0,
    '[user:mail]' => 0,
    '[user:created]' => 0,
    '[user:last-access]' => 0,
    '[user:last-login]' => 0,
    '[user:field_faculte_departement]' => 0,
    '[user:field-faculte-departement]' => 0,
    '[user:edit-url]' => 0,
    '[user:field_nom]' => 0,
    '[user:field-nom]' => 0,
    '[user:field_nom_complet]' => 0,
    '[user:field_numero_de_telephone]' => 0,
    '[user:field-numero-de-telephone]' => 0,
    '[user:field_prenom]' => 0,
    '[user:field-prenom]' => 0,
    '[user:name-raw]' => 0,
    '[user:name]' => 0,
    '[user:roles]' => 0,
    '[user:field_site_web]' => 0,
    '[user:field-statut]' => 0,
    '[user:status]' => 0,
    '[user:field_statut]' => 0,
    '[user:field-sujets-de-conferences-pres]' => 0,
    '[user:field_sujets_de_conferences_pres]' => 0,
    '[user:theme]' => 0,
    '[user:url]' => 0,
    '[user:cancel-url]' => 0,
    '[user:one-time-login-url]' => 0,
    '[user:validate-url]' => 0,
    '[user:original]' => 0,
    '[user:field__tablissement]' => 0,
    '[user:field--tablissement]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
