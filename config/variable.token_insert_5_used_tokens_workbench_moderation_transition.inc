<?php
/**
 * @file
 * variable.token_insert_5_used_tokens_workbench_moderation_transition.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_5_used_tokens_workbench_moderation_transition',
  'content' => array(
    '[workbench_moderation_transition:from-name]' => 0,
    '[workbench_moderation_transition:name]' => 0,
    '[workbench_moderation_transition:to-name]' => 0,
    '[workbench_moderation_transition:id]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
