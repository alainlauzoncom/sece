<?php
/**
 * @file
 * field.user.field_br_ve_presentation_de_l_ex.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'translatable' => '0',
    'entity_types' => array(),
    'settings' => array(),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_br_ve_presentation_de_l_ex' => array(
              'value' => 'field_br_ve_presentation_de_l_ex_value',
              'format' => 'field_br_ve_presentation_de_l_ex_format',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_br_ve_presentation_de_l_ex' => array(
              'value' => 'field_br_ve_presentation_de_l_ex_value',
              'format' => 'field_br_ve_presentation_de_l_ex_format',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => array(
      'format' => array(
        'table' => 'filter_format',
        'columns' => array(
          'format' => 'format',
        ),
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'field_name' => 'field_br_ve_presentation_de_l_ex',
    'type' => 'text_long',
    'module' => 'text',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => array(
      'value' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
      ),
      'format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
  ),
  'field_instance' => array(
    'label' => 'Brève présentation de l\'expertise',
    'widget' => array(
      'weight' => '11',
      'type' => 'text_textarea',
      'module' => 'text',
      'active' => 1,
      'settings' => array(
        'rows' => '5',
        'maxlength_js' => '280',
        'maxlength_js_enforce' => 1,
        'maxlength_js_truncate_html' => 0,
        'maxlength_js_label' => 'Veuillez décrire votre expertise générale, par mots-clés, en @limit caractères, il en reste : <strong>@remaining</strong>',
      ),
    ),
    'settings' => array(
      'text_processing' => '0',
      'user_register_form' => 1,
    ),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => '8',
        'settings' => array(
          'quickedit' => array(
            'editor' => 'plain_text',
          ),
        ),
        'module' => 'text',
      ),
    ),
    'required' => 1,
    'description' => '',
    'default_value' => NULL,
    'field_name' => 'field_br_ve_presentation_de_l_ex',
    'entity_type' => 'user',
    'bundle' => 'user',
    'deleted' => '0',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'text',
);
