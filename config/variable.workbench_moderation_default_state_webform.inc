<?php
/**
 * @file
 * variable.workbench_moderation_default_state_webform.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'workbench_moderation_default_state_webform',
  'content' => 'draft',
);

$dependencies = array();

$optional = array();

$modules = array();
