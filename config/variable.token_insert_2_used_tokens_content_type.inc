<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_content_type.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_content-type',
  'content' => array(
    '[content-type:node-count]' => 0,
    '[content-type:description]' => 0,
    '[content-type:edit-url]' => 0,
    '[content-type:name]' => 0,
    '[content-type:machine-name]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
