<?php
/**
 * @file
 * variable.menumenu_menu_de_gauchedexp_animate.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'menumenu-menu-de-gauchedexp_animate',
  'content' => 'fadeInDownBig',
);

$dependencies = array();

$optional = array();

$modules = array();
