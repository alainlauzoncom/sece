<?php
/**
 * @file
 * permission.eck_list_conf_rence_conf_rence_entities.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'eck list conf_rence conf_rence entities',
  'roles' => array(
    0 => 'administrator',
    1 => 'Professeur',
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'eck',
);
