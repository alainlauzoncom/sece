<?php
/**
 * @file
 * variable.user_mail_status_activated_subject.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'user_mail_status_activated_subject',
  'content' => 'Détails du compte [user:name-raw] sur [site:name] (approuvé)',
);

$dependencies = array();

$optional = array();

$modules = array();
