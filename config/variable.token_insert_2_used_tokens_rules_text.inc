<?php
/**
 * @file
 * variable.token_insert_2_used_tokens_rules_text.inc
 */

$api = '2.0.0';

$data = array(
  'name' => 'token_insert_2_used_tokens_rules_text',
  'content' => array(
    '[rules_text:value]' => 0,
  ),
);

$dependencies = array();

$optional = array();

$modules = array();
